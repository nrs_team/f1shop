/* 
 * @author NRS Team
 * @date 08/05/2018
 * @purpose control the items in cart
 */
var cartApp = angular.module("cartApp", []);
cartApp.controller("cartController", function ($scope, $http) {
    $scope.refreshCart = function (cartId) {
        $http.get('/f1shop/rest/cart/' + cartId).success(function (data) {
            $scope.cart = data;
        });
    }

    $scope.clearCart = function () {
        $http.delete("/f1shop/rest/cart/" + $scope.cartId).success($scope.refreshCart($scope.cartId));
        alert("Cart item deleted successfully");

    }
    $scope.initCartId = function (cartId) {
        $scope.cartId = cartId;
        $scope.refreshCart(cartId);
    }
    $scope.addToCart = function (productId) {
        
        $http.put('/f1shop/rest/cart/add/' + productId).success(function (data) {
            $scope.refreshCart($http.get('/f1shop/rest/cart/cartId'));
            alert("Product successfully added");
        });
    };
    $scope.instantAddToCart = function (productId) {
        $http.put('/f1shop/rest/cart/add/' + productId).success(function (data) {
            $scope.refreshCart($http.get('/f1shop/rest/cart/cartId'));
            alert("Product successfully added");
            $scope.initCartId(); 
        });
    };
        $scope.instantReduceCartItem = function (productId) {
        $http.put('/f1shop/rest/cart/reduce/' + productId).success(function (data) {
            //$scope.refreshCart($http.get('/f1shop/rest/cart/cartId'));
            alert("Product successfully added");
           // $scope.initCartId(); 
        });
    };
    
    $scope.removeFromCart = function (productId) {
        $http.put('/f1shop/rest/cart/remove/' + productId).success(function (data) {
            $scope.refreshCart($http.get('/f1shop/rest/cart/cartId'));
        });
    }

    $scope.totalItems = function () {
        $http.get('/f1shop/rest/cart/' + $scope.cartId).success(function (data) {
            $scope.cart = data;
        });
    }
});


