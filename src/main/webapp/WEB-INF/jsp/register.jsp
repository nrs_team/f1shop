<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Register New User: </h4>
    <hr>
    <form:form action="${pageContext.request.contextPath}/user/register" method="POST" commandName="userData">

        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">First Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.firstName" id="firstName"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="middleName" class="col-1 col-form-label">Middle Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.middleName" id="middleName"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="lastName" class="col-1 col-form-label">Last Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.lastName" id="lastName"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="userName" class="col-1 col-form-label">User Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="userName" id="userName"/>
                <a href="#" >Check Availability</a>
            </div>
        </div>
        <div class="form-group row">
            <label for="password" class="col-1 col-form-label">Password:</label>
            <div class="col-6">
                <form:input class="form-control" type="password" path="password" id="password"/>
            </div>
        </div>
<!--        <div class="form-group row">
            <label for="password" class="col-1 col-form-label">Retype Password:</label>
            <div class="col-6">
                <%--<form:input class="form-control" type="password" path="password" id="password"/>--%>
            </div>
        </div>-->
        <div class="form-group row">
            <label for="customerEmailId" class="col-1 col-form-label">Email:</label>
            <div class="col-6">
                <form:input class="form-control" type="email" path="customer.emailId" id="customerEmailId"/>
            </div>
        </div>
     
        <div class="form-group row">
            <div class="col-6">
                <button type="submit" id="submitBtn" class="btn btn-primary">Create</button>
            </div>
            <div class="col-6">
                <a href="<c:url value="/index"/>" class="btn btn-danger">Cancel</a>
            </div>

        </div>
    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>