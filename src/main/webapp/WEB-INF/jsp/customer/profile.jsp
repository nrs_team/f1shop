<%@ include file="/WEB-INF/jsp/includes/header.jsp" %>
<div class="container">
    <h4>Customer Profile: </h4>
    <hr>
    <form:form action="${pageContext.request.contextPath}/customer/profile/${userdata.id}" method="POST" commandName="userdata">
        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">First Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.firstName" />
            </div>
        </div>

        <div class="form-group row">
            <label for="middleName" class="col-1 col-form-label">Middle Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.middleName" />
            </div>
        </div>

        <div class="form-group row">
            <label for="lastName" class="col-1 col-form-label">Last Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.lastName" />
            </div>
        </div>

        <div class="form-group row">
            <label for="lastName" class="col-1 col-form-label">User Name:</label>

            <div class="col-6">
                <input type="text" id="username" name="username" class="form-control" placeholder="User Name" />
            </div>
        </div>

        <div class="form-group row">
            <label for="lastName" class="col-1 col-form-label">Password:</label>
            <div class="col-6">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" />
            </div>
        </div>

        <div class="form-group row">
            <label for="emailId" class="col-1 col-form-label">Email ID:</label>
            <div class="col-6">
                <form:input class="form-control" type="email" path="customer.emailId" />
            </div>
        </div>

        <div class="form-group row">
            <label for="mobileNumber" class="col-1 col-form-label">Contact Number:</label>
            <div class="col-6">
                <form:input class="form-control" type="number" path="customer.mobile" />
            </div>
        </div>

        <div class="form-group row">
            <label for="religion" class="col-1 col-form-label">Religion:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.religion" />
            </div>
        </div>

        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">Date of Birth:</label>
            <div class="col-6">
                <form:input class="form-control" type="date" path="customer.dateOfBirth" />
            </div>
        </div>

        <div class="form-group row">
            <label class="col-1 col-form-label">Gender</label>
            <div class="col-xs-6">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default">
                        <input type="radio" name="gender" value="male" /> Male
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="gender" value="female" /> Female
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="gender" value="other" /> Other
                    </label>
                </div>
            </div>
            <!--            <label for="firstName" class="col-1 col-form-label">Gender:</label>
                        <div class="col-6">
            <form:input class="form-control" type="text" path="customer.gender" />
        </div>-->
        </div>

        <!--        <div class="form-group row">
                    <label for="firstName" class="col-1 col-form-label">Profile Image:</label>
                    <div class="col-6">
        <form:input class="form-control" type="text" path="customer.imageUrl" />
    </div>
</div>-->
        <div class="form-group row">
            <label class="col-l col-form-label text-nowrap">Image Url:  </label>
            <div class="col-10">
                <input type = "file" name = "productImage" path="productImage" class="image-name btn" size = "50"  />
            </div>
            <div class="col-2">
                <img id="thumb" src="../../../assests/image/rectangle.png" alt="your image" height="100" />
            </div>
        </div>

        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">Address Line 1:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.addressOne" />
            </div>
        </div>


        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">Address Line 2:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.addressTwo" />
            </div>
        </div>


        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">Country:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.country" />
            </div>
        </div>

        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">City:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.city" />
            </div>
        </div>


        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">State:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.state" />
            </div>
        </div>


        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">Postcode:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.postCode" />
            </div>
        </div>

        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">Shipping:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.isShipping" />
            </div>
        </div>

        <div class="form-group row">
            <label for="firstName" class="col-1 col-form-label">Billing:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="customer.address.isBilling" />
            </div>
        </div>

        <div class="form-group row">
            <div class="col-6">
                <button type="submit" id="submitBtn" class="btn btn-danger">Save</button>
            </div>
            <div class="col-6">
                <a href="<c:url value="/index"/>" class="btn btn-primary">Cancel</a>
            </div>

        </div>

    </form:form>
</div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>
