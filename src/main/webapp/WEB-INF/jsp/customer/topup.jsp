<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Top-up Amount: </h4>
    <hr>
    <form:form action="${pageContext.request.contextPath}/wallet/toppedup" method="POST" commandName="walletTransaction">
        <div class="form-group row">
            <label for="Amount" class="col-2 col-form-label text-nowrap">Enter Amount</label>
            <div class="col-3">
                <form:input class="form-control" type="number" path="amount" id="amount"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="Amount" class="col-2 col-form-label text-nowrap">Card Number</label>
        <div class="col-3">
                <form:input class="form-control" type="number" path="cardNumber" id="amount"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="Amount" class="col-2 col-form-label text-nowrap">Expiry Date</label>
            <div class="col-3">
                <input class="form-control" type="date"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="Amount" class="col-2 col-form-label text-nowrap">Cvv</label>
            <div class="col-3">
                <input class="form-control" type="number" />
            </div>
        </div>
        <div class="form-group row">
            <div class="col-2">
                <button type="submit" id="submitBtn" class="btn btn-danger">Add</button>
            </div>
            <div class="col-2">
                <a href="<c:url value="#"/>" class="btn btn-primary">Cancel</a>
            </div>

        </div>
    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>