<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<main role="main">
    <div class="container">
        <div class="col-md-12">
            <h4>Discounts</h4>
            <hr>
            <form:form action="${pageContext.request.contextPath}/admin/discountSearch" method="POST" commandName="discount">
                <div class="form-group row">
                    <input class="form-control col-sm-4" type="text" placeholder="Search" aria-label="Search" 
                            name="searchedDiscountName"/>
                    <button class="btn btn-outline-danger col-sm-2" type="submit">Search</button>
                    <a href="<spring:url value="discount/create"/>" class="btn btn-primary" >Create New Discount</a>
                </div>
                <div class="form-group row">
                    <c:if test = "${filter > ''}">       
                        <a href="<spring:url value="/admin/discounts"/>" class="btn btn-default" title="Click to reset" >
                            <label class="btn btn-info"><span class="tooltiptext">Search Result for: ${filter}</span> </label></a>  
                        </c:if>
                </div>
            </form:form>

            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <th><input type="checkbox" id="checkall" /></th>
                    <th>Discount Name</th>
                    <th>Percentage</th>
                    <th>Description</th>
                    <th></th>
                    </thead>
                    <tbody>
                        <c:forEach items="${discountList}" var="discount">
                            <tr>
                                <td><input type="checkbox" class="checkthis" /></td>
                                <td>${discount.name}</td>
                                <td>${discount.percentage}</td>
                                <td>${discount.remarks}</td> 
                                <td class="btn-group mr-2">

                                    <a href="<spring:url value="discount/view/${discount.id}" />"> 
                                        <p data-placement="top" data-toggle="tooltip" title="Detail">
                                            <button class="btn btn-success btn-xs" data-title="Detail" data-toggle="modal" data-target="#Detail" >
                                                <span class="fa fa-info"></span>
                                            </button>
                                        </p>
                                    </a>
v
                                    <a href="<spring:url value="discount/edit/${discount.id}" />"> 
                                        <p data-placement="top" data-toggle="tooltip" title="Edit">
                                            <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" >
                                                <span class="fa fa-pencil"></span>
                                            </button>
                                        </p>
                                    </a>
                                    <a href="<spring:url value="discount/delete/${discount.id}" />">
                                        <p data-placement="top" data-toggle="tooltip" title="Set Inactive">
                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" >
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </p>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</main>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>