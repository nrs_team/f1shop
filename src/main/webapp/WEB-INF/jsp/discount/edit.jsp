<%-- 
    Document   : index
    Created on : Apr 13, 2018, 7:31:56 PM
    Author     : Sunil Tako
--%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<main role="main">
    <div class="container-wrapper">
        <div class="container">
            <form:form action="${pageContext.request.contextPath}/admin/discount/edit/${discount.id}" method="POST" commandName="discount" >
                <div class="form-group row">
                    <label for="name">Name:</label>
                    <form:input path="name" type="text" class="form-control" name="discountName"/>
                </div>
                <div class="form-group row">
                    <label for="percentage">Percentage:</label>
                    <form:input path="percentage" class="form-control" name="discountPercentage"/>
                </div>
                <div class="form-group row">
                    <label for="description">Description:</label>
                    <form:textarea path="remarks" class="form-control" rows="5" name="description"/>
                </div>
                <div class="form-group row">
                    <label> Status: 
                        <span class="checkbox"><form:checkbox path="isActive"/></span>
                    </label>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" >Save changes</button>
                    <a href="<c:url value="/admin/discounts"/>" class="btn btn-default">Cancel </a>
                </div>
            </form:form>
        </div>
    </div>
</main>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>