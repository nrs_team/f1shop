<%-- 
    Document   : view
    Created on : Apr 28, 2018, 5:04:53 PM
    Author     : NRS Team
--%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Discount Details: </h4> 
    <form:form action="" method="POST" commandName="discount">
        <div class="form-group row">
            <label for="name" class="col-1 col-form-label text-nowrap">Name  <span class="fa fa-address-book">: </span></label>
            <div class="col-6">
                <label for="name" class="col-1 col-form-label text-nowrap"> ${discount.name}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label">Email <span class="fa fa-envelope">: </span> </label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${discount.percentage}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label">Mobile <span class="fa fa-mobile">: </span></label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${discount.remarks}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<spring:url value="/admin/discounts"/>" class="btn btn-success" >Cancel</a>
                <a href="<spring:url value="/admin/discount/edit/${discount.id}"/>" class="btn btn-primary" >Edit</a>
                <a href="<spring:url value="/admin/discount/delete/${discount.id}"/>" class="btn btn-danger" >Set Inactive</a>
            </div>
        </div>
    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>
