<%-- 
    Document   : addDiscount
    Created on : Apr 17, 2018, 12:45:24 AM
    Author     : NRS-TEAM
--%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<main role="main">
    <div class="container">
        <h4>Create new Discount</h4>
        <hr>
        <form:form action="${pageContext.request.contextPath}/admin/discount/create" method="POST" commandName="discount">
            <div class="form-group row">
                <label for="name" class="col-1 col-form-label">Name:</label>
                <form:input path="name" class="form-control"/>
            </div>
            <div class="form-group row">
                <label for="percentage" class="col-1 col-form-label">Percentage:</label>
                <form:input path="percentage" class="form-control"/>
            </div>
            <div class="form-group row">
                <label for="description" class="col-1 col-form-label">Description:</label>
                <form:textarea  path="remarks" class="form-control" rows="5"/>
            </div>
            <div class="form-group row">
                <div class="col-1">
                    <button type="submit" id="submitBtn" class="btn btn-danger">Create</button>
                </div>
                <div class="col-1">
                    <a href="<c:url value="/admin/discounts"/>" class="btn btn-primary">Cancel</a>
                </div>

            </div>
        </form:form>
    </div>
</main>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>