<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <div class="col-md-12">
        <h4>User List</h4>

        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>
                <th><input type="checkbox" id="checkall" /></th>
                <th>Full Name</th>
                <th>Address</th>
                <th>User Id</th>
                <th></th>
                <th></th>
                </thead>
                <tbody>
                    <c:forEach items="${userList}" var="userdata">
                        <tr>
                            <td><input type="checkbox" class="checkthis" /></td>
                            <td>${userdata.customer.firstName} ${userdata.customer.middleName} ${userdata.customer.lastName}</td>
                            <td>${userdata.customer.address.addressOne}</td>
                            <td>${userdata.id}</td> 
                            <td class="btn-group mr-2">

                                <!--<a href="<spring:url value="supplier/view/${userdata.id}" />">--> 
                                <p data-placement="top" data-toggle="tooltip" title="Detail">
                                    <button class="btn btn-success btn-xs" data-title="Detail" data-toggle="modal" data-target="#${supplier.id}" >
                                        <span class="fa fa-info"></span>
                                    </button>
                                </p>
                                <!--                                </a>-->

                                <a href="<spring:url value="supplier/edit/${userdata.id}" />"> 
                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                        <button class="btn btn-primary btn-xs" data-title="Edit">
                                            <span class="fa fa-pencil"></span>
                                        </button>
                                    </p>
                                </a>
                                <a href="<spring:url value="supplier/delete/${userdata.id}" />">
                                    <p data-placement="top" data-toggle="tooltip" title="Set Inactive">
                                        <button class="btn btn-danger btn-xs" data-title="Delete" >
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </p>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>