<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="row row-m-t"> 
    <div class="container">
        <div class="col-md-12">
            <h4>Transaction List</h4>
            <form:form class="form-inline mt-2 mt-md-0" action="${pageContext.request.contextPath}/admin/orderDetail/search" method="POST">
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">

                            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" name="name" path="name"/>
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>

                        </div>
                        <div class="col-md-6 text-right">
                            <!--<a href="<spring:url value="/admin/addCategory" ></spring:url>" class="btn btn-primary pull-right" >Create New Category</a>-->
                            </div>


                        </div>
                    </div>
            </form:form>

            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <th><input type="checkbox" id="checkall" /></th>
                    <th>Order Id</th>
                    <th>Customer Name</th>
                    <th>Product Name</th>
                    <th>Buying Price</th>
                    <th>Status</th>

                    </thead>
                    <tbody>
                        <c:forEach items="${orderItemList}" var="orderItem">
                            <tr>
                                <td><input type="checkbox" class="checkthis" /></td>
                                <td>
                                    <a href="<spring:url value="orderDetail/${orderItem.id}" />">${orderItem.id}</a>
                                </td>
                                <td>${orderItem.orderDetail.customer.firstName} ${orderItem.orderDetail.customer.lastName}</td>
                                <td>${orderItem.product.name}</td>
                                <td>${orderItem.buyingPrice}</td>
                                <td class="btn btn-group">

                                    <a href="<spring:url value="/admin/viewCategory/${orderDetail.id}" />"> 
                                        <p data-placement="top" data-toggle="tooltip" title="Detail">
                                            <button class="btn btn-success btn-xs" data-title="Detail" data-toggle="modal" data-target="#Detail" >
                                                <span class="fa fa-info"></span>
                                            </button>
                                        </p>
                                    </a>
                                    <a href="<spring:url value="/admin/editCategory/${orderDetail.id}" />">
                                        <p data-placement="top" data-toggle="tooltip" title="Edit">
                                            <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" >
                                                <span class="fa fa-pencil"></span>
                                            </button>
                                        </p>
                                    </a>
                                    <a href="<spring:url value="/admin/deleteCategory/${orderDetail.id}" />">
                                        <p data-placement="top" data-toggle="tooltip" title="Delete">
                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" >
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </p>
                                    </a>

                                </td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="clearfix"></div>
                <ul class="pagination pull-right">
                    <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                </ul>

            </div>

        </div>
    </div>
</div>

<div id="editCategory" name="editCategory" title="Edit Category" style="display: none; overflow: auto;">
    <center>
        <p>Click on "Save" to Save your changes.</p>
    </center>
</div>


<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>
