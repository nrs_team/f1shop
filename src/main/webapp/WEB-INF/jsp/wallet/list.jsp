<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <div class="col-md-12">
        <h4>Suppliers</h4>
        <hr>    
        <!--implement search here-->
        <form:form action="${pageContext.request.contextPath}/admin/wallet/search" method="GET" commandName="wallet">
            <div class="form-group row">
                <input class="form-control col-sm-4" type="text" placeholder="Search" aria-label="Search" 
                       id="searchedSupplierName" name="searchCustomer"/>
                <button class="btn btn-outline-danger col-sm-2" type="submit">Search</button>
                <a href="<spring:url value="wallet/create"/>" class="btn btn-primary" >Add new wallet</a>
            </div>
            <div class="form-group row">
                <c:if test = "${filter > ''}">       
                    <a href="<spring:url value="/admin/wallet"/>" class="btn btn-default" title="Click to reset" >
                        <label class="btn btn-info"><span class="badge">Filter applied: ${filter}</span> </label></a>  
                    </c:if>
            </div>
        </form:form>

        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>
                <th><input type="checkbox" id="checkall" /></th>
                <th>Customer Name</th>
                <th>Address</th>
                <th>Available Balance</th>
                <th></th>
                <th></th>
                </thead>
                <tbody>
                    <c:forEach items="${walletList}" var="wallet">
                        <tr>
                            <td><input type="checkbox" class="checkthis" /></td>
                            <td>${wallet.customer.firstName} ${wallet.customer.middleName} ${wallet.customer.lastName}</td>
                            <td>${wallet.customer.address.addressOne}</td>
                            <td>${wallet.availableBalance}</td> 
                            <td class="btn-group mr-2">

                                <!--<a href="<spring:url value="wallet/view/${wallet.id}" />">--> 
                                <p data-placement="top" data-toggle="tooltip" title="Detail">
                                    <button class="btn btn-success btn-xs" data-title="Detail" data-toggle="modal" data-target="#${wallet.id}" >
                                        <span class="fa fa-info"></span>
                                    </button>
                                </p>
                                <!--                                </a>-->

                                <a href="<spring:url value="wallet/edit/${wallet.id}" />"> 
                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                        <button class="btn btn-primary btn-xs" data-title="Edit" disabled>
                                            <span class="fa fa-pencil"></span>
                                        </button>
                                    </p>
                                </a>
                                <a href="<spring:url value="wallet/delete/${wallet.id}" />">
                                    <p data-placement="top" data-toggle="tooltip" title="Set Inactive">
                                        <button class="btn btn-danger btn-xs" data-title="Delete" disabled >
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </p>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>