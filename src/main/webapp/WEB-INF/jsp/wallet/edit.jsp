<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Create new supplier: </h4>
    <hr>
    <form:form action="${pageContext.request.contextPath}/admin/wallet/update/${wallet.id}" method="POST" commandName="wallet">

        <div class="form-group row">
            <label for="category" class="col-2 col-form-label text-nowrap">Customer Name: </label>
            <div class="col-6">
               <form:select path="customerId">
                   <form:option value="" label="--- Select Customer---" />
                   <form:options items="${customerList}" itemValue="id" itemLabel="firstName"/>
               </form:select>
            </div>
        </div>
        
        <div class="form-group row">
            <label for="Amount" class="col-2 col-form-label text-nowrap">Enter Amount</label>
            <div class="col-3">
                <form:input class="form-control" type="number" path="availableBalance" id="amount"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-2">
                <button type="submit" id="submitBtn" class="btn btn-danger">Add</button>
            </div>
            <div class="col-2">
                <a href="<c:url value="/admin/wallets"/>" class="btn btn-primary">Cancel</a>
            </div>

        </div>
    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>