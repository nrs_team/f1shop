<%-- 
    Document   : index
    Created on : Apr 13, 2018, 7:31:56 PM
    Author     : Sunil Tako
--%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>



<main role="main">
    <div class="row row-m-t"> 
        <div class="container">
            <div class="col-md-12">
                <h4>Managing all Elements</h4>

                <h1>Administration Panel</h1>
                
                    <button class="btn-lg btn-primary my-2 my-sm-0">Search</button>

                <ul><a href="/f1shop/admin/customers"> Manage All Customers</a></ul> 
                <ul><a href="/f1shop/admin/products"> Manage All Products</a></ul>
                <ul><a href="/f1shop/admin/categories"> Manage All Categories</a></ul>
                <ul><a href="/f1shop/admin/discounts"> Manage All Discounts</a></ul>

            </div>

        </div>
    </div>

    <!-- FOOTER -->
    <footer class="container">
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017-2018 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
    </footer>
</main>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>');</script>
<script src="/assests/js/popper.min.js"></script>
<script src="/assests/js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<!-- <script src="../../../../assets/js/vendor/holder.min.js"></script> -->
</body>
</html>


