<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <div class="col-md-12">
        <h4>Suppliers</h4>
        <hr>    
        <!--implement search here-->
        <form:form action="${pageContext.request.contextPath}/admin/supplier/search" method="GET" commandName="supplier">
            <div class="form-group row">
                <input class="form-control col-sm-4" type="text" placeholder="Search" aria-label="Search" 
                       id="searchedSupplierName" name="searchedSupplierName"/>
                <button class="btn btn-outline-danger col-sm-2" type="submit">Search</button>
                <a href="<spring:url value="supplier/create"/>" class="btn btn-primary" >Create New Supplier</a>
            </div>
            <div class="form-group row">
                <c:if test = "${filter > ''}">       
                    <a href="<spring:url value="/admin/suppliers"/>" class="btn btn-default" title="Click to reset" >
                        <label class="btn btn-info"><span class="badge">Filter applied: ${filter}</span> </label></a>  
                    </c:if>
            </div>
        </form:form>

        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>
                <th><input type="checkbox" id="checkall" /></th>
                <th>Supplier Name</th>
                <th>Address</th>
                <th>Mobile No</th>
                <th></th>
                <th></th>
                </thead>
                <tbody>
                    <c:forEach items="${supplierList}" var="supplier">
                        <tr>
                            <td><input type="checkbox" class="checkthis" /></td>
                            <td>${supplier.name}</td>
                            <td>${supplier.address.addressOne}</td>
                            <td>${supplier.mobile}</td> 
                            <td class="btn-group mr-2">

                                <!--<a href="<spring:url value="supplier/view/${supplier.id}" />">--> 
                                <p data-placement="top" data-toggle="tooltip" title="Detail">
                                    <button class="btn btn-success btn-xs" data-title="Detail" data-toggle="modal" data-target="#${supplier.id}" >
                                        <span class="fa fa-info"></span>
                                    </button>
                                </p>
                                <!--                                </a>-->

                                <a href="<spring:url value="supplier/edit/${supplier.id}" />"> 
                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                        <button class="btn btn-primary btn-xs" data-title="Edit">
                                            <span class="fa fa-pencil"></span>
                                        </button>
                                    </p>
                                </a>
                                <a href="<spring:url value="supplier/delete/${supplier.id}" />">
                                    <p data-placement="top" data-toggle="tooltip" title="Set Inactive">
                                        <button class="btn btn-danger btn-xs" data-title="Delete" >
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </p>
                                </a>
                            </td>
                            <!--below td element is used to display all details when user click detail icon above-->
                            <td>
                                <div class="modal fade product_view" id="${supplier.id}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <a href="#" data-dismiss="modal" class="class pull-right"><span class="fa fa-remove"></span></a>
                                                <h3><i class="badge">Supplier Details</i></h3> 
                                            </div>
                                            <div class="modal-body">               
                                                <div class="form-group row">
                                                    <label for="supplierName" class="col-1 col-form-label text-nowrap">Name  <span class="fa fa-address-book">: </span></label>
                                                    <div class="col-6">
                                                        <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.name}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="supplierName" class="col-1 col-form-label text-nowrap">Email <span class="fa fa-envelope">: </span> </label>
                                                    <div class="col-6">
                                                        <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.emailId}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="supplierName" class="col-1 col-form-label text-nowrap">Mobile <span class="fa fa-mobile">: </span></label>
                                                    <div class="col-6">
                                                        <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.mobile}</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="supplierName" class="col-1 col-form-label text-nowrap">Address: </label>
                                                    <div class="col-6">
                                                        <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.address.addressOne}, ${supplier.address.addressTwo} - ${supplier.address.state} ${supplier.address.postCode} </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="supplierName" class="col-1 col-form-label text-nowrap">Remarks: </label>
                                                    <div class="col-6">
                                                        <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.name}</label>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <a href="<spring:url value="/admin/suppliers"/>" class="btn btn-success" >Cancel</a>
                                                        <a href="<spring:url value="/admin/supplier/edit/${supplier.id}"/>" class="btn btn-primary" >Edit</a>
                                                        <a href="<spring:url value="/admin/supplier/delete/${supplier.id}"/>" class="btn btn-danger" >Set Inactive</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>