<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Edit supplier: </h4> 
    <hr>
    <form:form action="${pageContext.request.contextPath}/admin/supplier/edit/${supplier.id}" method="POST" commandName="supplier">

        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label">Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="name" id="supplierName"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierEmail" class="col-1 col-form-label">Email:</label>
            <div class="col-6">
                <form:input class="form-control" type="email" path="emailId" id="supplierEmail"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierMobile" class="col-1 col-form-label">Mobile:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="mobile" id="supplierMobile"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="address1" class="col-1 col-form-label text-nowrap">Address 1: </label>
            <div class="col-6">
                 <form:input class="form-control" type="text" path="address.addressOne" id="address1"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="address2" class="col-1 col-form-label text-nowrap">Address 2: </label>
            <div class="col-6">
                 <form:input class="form-control" type="text" path="address.addressTwo" id="address2"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="city" class="col-1 col-form-label">City: </label>
            <div class="col-2">
                <form:input class="form-control" type="text" path="address.city" id="city"/>
            </div>
            <label for="state" class="col-1 col-form-label">State: </label>
            <div class="col-2">
                <form:input class="form-control" type="text" path="address.state" id="state"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="postCode" class="col-1 col-form-label text-nowrap">Post Code: </label>
            <div class="col-2">
                <form:input class="form-control" type="number" path="address.postCode" id="postCode"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="remarks" class="col-1 col-form-label">Remarks: </label>
            <div class="col-6">
               <form:input class="form-control" type="text" path="remarks" id="remarks"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="submitBtn" class="col-2 col-form-label text-nowrap">Upload Id/Photo:  </label>
            <div class="col-10">
                <input type = "file" name = "file" size = "50" class="btn" />
            </div>
        </div>
        <div class="form-group row">
            <div class="col-1">
                <button type="submit" id="submitBtn" class="btn btn-danger">Update</button>
            </div>
            <div class="col-1">
                <a href="<c:url value="/admin/suppliers"/>" class="btn btn-primary">Cancel</a>
            </div>

        </div>

    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>