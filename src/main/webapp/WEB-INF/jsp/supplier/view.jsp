<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Supplier Details: </h4> 
    <form:form action="" method="POST" commandName="supplier">
        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label text-nowrap">Name  <span class="fa fa-address-book">: </span></label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.name}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label">Email <span class="fa fa-envelope">: </span> </label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.emailId}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label">Mobile <span class="fa fa-mobile">: </span></label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.mobile}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label text-nowrap">Address <span class="fa fa-address-card">: </label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.address.addressOne}, ${supplier.address.addressTwo} - ${supplier.address.state} ${supplier.address.postCode} </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label">Remarks: </label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${supplier.name}</label>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<spring:url value="/admin/suppliers"/>" class="btn btn-success" >Cancel</a>
                <a href="<spring:url value="/admin/supplier/edit/${supplier.id}"/>" class="btn btn-primary" >Edit</a>
                <a href="<spring:url value="/admin/supplier/delete/${supplier.id}"/>" class="btn btn-danger" >Set Inactive</a>
            </div>
        </div>
    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>