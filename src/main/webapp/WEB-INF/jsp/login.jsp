<%-- 
    Document   : login
    Created on : Apr 30, 2018, 5:30:29 PM
    Author     : NRS Team
--%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container-wrapper">

    <div class="container">
        <div id="login-box" class="alert alert-info col-sm-6">
            <h2>Login with your credential</h2>

            <c:if test="${not empty msg}">
                <div class="msg"> ${msg} </div>
            </c:if>
                
            <form name="loginForm" action="<c:url value="/j_spring_security_check" />" method="post">
                <c:if test="${not empty error}">
                    <div class="error" style="color:#ff0000">${error}</div>
                </c:if>
                <div class="form-group">
                    <input type="text" id="username" name="username" class="form-control" placeholder="User Name" />
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" />
                </div>

                <input type="submit" value="Submit" class="btn btn-primary"/>
                <a href="<spring:url value="/index"/>" class="btn btn-danger">Cancel</a>
                <input type="hidden" name="${_csrf.parameterName}" value ="${_csrf.token}" />

            </form>
        </div>
    </div>

</div>