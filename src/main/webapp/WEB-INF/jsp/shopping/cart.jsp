<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container-wrapper" ng-app="cartApp" scope="page">
    <div class="container">
        <section>
            <div class="heading">
                <div class="container">
                    <h4 class="alert alert-success">Welcome to your cart {{cart.customerName}}</h4>
                </div>
            </div> 
        </section>
        <section class="container">
            <div ng-controller="cartController" ng-init="initCartId('${cartId}')">
                <div>
                    <a class="btn btn-danger pull-left" ng-click="clearCart()"><span class="fa fa-remove"></span> Clear cart</a>
                </div>
                <table class="table table-hover">
                    <tr>
                        <th>Item Name</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                    <tr ng-repeat="item in cart.cartItems"> 
                   
                        <td>{{item.product.name}}</td>
                        <td><span id="currency-default">{{item.product.unitPrice|currency}}</span></td>
                        <td>{{item.quantity}} 
                            
                            <!--<input type="number" ng-model="item.quantity" aria-label="quantity" ng-change="instantAddToCart(item.product.id)">-->
                            <a href="<spring:url value="/shopping/cart"/>" class="btn btn-success" ng-click="instantAddToCart(item.product.id)" ng-model="item.product.id">
                                <span class="fa fa-plus"></span>                          
                            </a> 
                            <a href="<spring:url value="/shopping/cart"/>" class="btn btn-danger" ng-click="instantReduceCartItem(item.product.id)" ng-model="item.product.id">
                                <span class="fa fa-minus"></span>                          
                            </a>
                        </td> 
                        <td><span id="currency-default">{{item.quantity * item.product.unitPrice|currency}}</span></td>
                        <td>
                            <a href="<spring:url value="/shopping/cart"/>" class="btn btn-danger" ng-click="removeFromCart(item.product.id)">
                                <span class="fa fa-remove"></span>Remove                                
                            </a> 
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Total items: {{cart.totalItemCount}}</th>
                        <th>Grand Total: <span id="currency-default">{{cart.grandTotal|currency}}</span></th>
                        <th></th>
                    </tr>
                    
                </table>

                <a href="<spring:url value="/shopping"/>" class="btn btn-primary">Continue shopping</a>
                
                <a href="<spring:url value="/shopping/cart/checkout/${cartId}"/>" class="btn btn-success">  <span class="fa fa-credit-card"></span> Checkout</a>

            </div>
        </section>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>