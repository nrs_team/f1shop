<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container" ng-app="cartApp">
     <form:form action="${pageContext.request.contextPath}/shopping/search" method="POST">
        <div class="form-group row">
            <input class="form-control col-sm-4" type="text" placeholder="Search" aria-label="Search" 
                   id="searchedProductName" name="searchedProductName"/>
            <input type="hidden" name="categoryId" value="${categoryId}" />
            <button class="btn btn-outline-danger col-sm-2" type="submit">Search</button>
            <a href="<spring:url value="shopping/cart"/>" 
               class="btn btn-primary"><span class="fa fa-eye"></span> View Cart</a>
        </div>
        
        <div class="form-group row">
            <c:if test = "${filter > ''}">       
                <a href="<spring:url value="/admin/suppliers"/>" class="btn btn-default" title="Click to reset" >
                    <label class="btn btn-info"><span class="badge">Filter applied: ${filter}</span> </label></a>  
                </c:if>
        </div>
    </form:form>

    <div class="row">
        <c:set var="count" value="0" scope="page" />
        <c:forEach items="${productList}" var="product">
            <div class="col-md-3">
                <span class="heading">${product.name}</span> <i class="badge" data-toggle="modal" data-target="#${product.id}"> Price: ${product.unitPrice}</i>
                <div class="thumbnail">
                    <img class="img-responsive" src="<c:url value="/assests/image/product/${product.id}.png"/>" alt="Generic placeholder image" width="70" height="140">
                    <div class="caption">
                        <p>${product.description}</p>
                    </div>
                    <div class="ratings">
                        <p>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            (${product.views} views)
                        </p>
                    </div>
                    <div class="space-ten"></div>
                    <br>

                    <!--userrole variable should be sent from controller for additional checking here-->
                    <c:set var="role" scope="page" value="${userrole}"> </c:set>
                        <div class="btn-ground text-center text-nowrap">
                            <p ng-controller="cartController">
                                <a href="" class="btn btn-warning" ng-click="addToCart('${product.id}')"><span class="fa fa-shopping-cart"></span> Add to cart</a>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#${product.id}"><i class="fa fa-search"></i> View Detail</button>

                        </p>
                        <!--                        <button type="button" class="btn btn-danger"><i class="fa fa-shopping-cart"></i> Add To Cart</button>-->
                    </div>
                    <div class="space-ten"></div>
                </div>
            </div>
            <div class="modal fade product_view" id="${product.id}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <a href="#" data-dismiss="modal" class="class pull-right"><span class="fa fa-remove"></span></a>
                            <h3 class="modal-title">${product.name}</h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 product_img">
                                    <img src="<c:url value="/assests/image/product/${product.id}.png"/>" class="img-responsive">
                                </div>
                                <div class="col-md-6 product_content">
                                    <h4>Product Id: <span>${product.id}</span></h4>
                                    <div class="rating">
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        (${product.views} views)
                                    </div>
                                    <p>${product.description}</p>

                                    <div class="space-ten"></div>
                                    <div class="btn-ground">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <c:set var="count" value="${count + 1}" scope="page"/>
            <c:if test="${count}%4==0 "> 
            </div>
            <div class="row">
            </c:if>
        </c:forEach>
    </div>

</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>