<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container-wrapper" ng-app="cartApp" ng-controller="cartController" ng-init="initCartId('${cartId}')" scope="page">
    <div class="container">
        <section>
            <div class="heading">
                <div class="container alert alert-success">
                    <h4>Welcome to Cart Checkout {{cart.customerName}}</h4>
                    <h6>Your current balance is: <span id="currency-default">{{cart.walletBalance|currency}}</span> <a href="${pageContext.request.contextPath}/admin/wallet/create" class="btn btn-primary pull-right" >Topup wallet</a></h6>
                    <h3>{{message}}</h3>
                </div>
            </div> 
        </section>
        <div class="alert alert-success alert-dismissible" role="alert" style="display: none" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3>{{message}}</h3>
        </div>
        <!--<section><%@ include file="/WEB-INF/jsp/includes/message.jsp"%>  </section>-->          
        <section class="container">
            <div>
                <table class="table table-hover">
                    <tr>
                        <th>Item Name</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                    <tr ng-repeat="item in cart.cartItems">    
                        <td><span id="currency-default">{{item.product.name}}</span> </td>
                        <td><span id="currency-default">{{item.product.unitPrice|currency}}</span></td>
                        <td><span id="currency-default">{{item.quantity}}</span></td> 
                        <td><span id="currency-default">{{item.quantity * item.product.unitPrice|currency}}</span></td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Grand Total</th>
                        <th><span id="currency-default">{{cart.grandTotal|currency}}</span></th>

                    </tr>
                    <tr>
                        <th>
                            <!--{{(cart.walletBalance - cart.grandTotal) > 0}}-->
                        </th>
                        <th></th>
                        <th>Available wallet balance: </th>
                        <!--need to get this information from customer check the authentication as well in this level -->
                        <th><span id="currency-default">{{cart.walletBalance|currency}}</span></th> 

                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>New balance after purchase:</th>
                        <th><span id="currency-default">{{cart.walletBalance - cart.grandTotal|currency}}</span></th> 
                    </tr>
                </table>

                <div>
                    <form:form action="${pageContext.request.contextPath}/shopping/checkout/${cartId}" method="GET" commandName="supplier">
                        <button class="btn btn-danger" type="submit">  <span class="fa fa-credit-card"></span>Pay</button>
                        <!--<a href="<spring:url value="/checkout"/>" class="btn btn-danger"> <span class="fa fa-credit-card"></span> Pay</a>-->
                        <a href="<spring:url value="/shopping"/>" class="btn btn-success"> Cancel</a>
                    </form:form>
                </div>

            </div>
        </section>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>