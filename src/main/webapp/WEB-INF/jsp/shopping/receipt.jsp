<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <img alt="Bootstrap Image Preview" height="100" width="200" src="<c:url value="/assests/image/logo.png" />" class="pull-right" />
                        </div>
                        <div class="col-md-6">

                            <address>
                                <strong>Auburn Bazar</strong>
                                <br /> 400 Kent Street<br />
                                Sydney, NSW<br /> 
                                <abbr title="Phone">P:</abbr> (06) 02-0215632
                            </address>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">

                            <address>

                                <strong>Billing Address:</strong>
                                <br /> 
                                ${userData.customer.address.addressOne}, ${userData.customer.address.addressTwo}<br /> 
                                ${userData.customer.address.state}, ${userData.customer.address.postCode}
                                <br />
                                <abbr title="Phone">P:</abbr> ${userData.customer.mobile}
                            </address>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <span>Date : </span> <fmt:formatDate value="${orderDetail.createdDate}" pattern="yyyy-MM-dd HH:mm:ss" />
                </div>
                <div class="col-md-6">
                    <button style="float: right" class="btn btn-primary my-2 my-sm-0" type="submit">Print Receipt</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    S.N.
                                </th>
                                <th>
                                    Particulars
                                </th>
                                <th>
                                    Quantity
                                </th>
                                <th>
                                    Amount
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach items="${orderItemList}" var="item" varStatus="theCount">   
                                <tr>
                                    <td>
                                        ${theCount.count}
                                    </td>
                                    <td>
                                        ${item.product.name}
                                    </td>
                                    <td>
                                        ${item.itemCount}
                                    </td>
                                    <td>
                                        ${item.product.unitPrice}
                                    </td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="3">
                                    <h3 class="text-center">	Total </h3>

                                </td>
                                <td>
                                    ${orderDetail.totalPrice}
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>