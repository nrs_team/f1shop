<hr>
<footer class="container">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2017-2018 NRS Team, Adding value in digital world. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#thumb').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".image-name").change(function () {
        readURL(this);
    });
</script>



</body>
</html>
