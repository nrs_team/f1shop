<form class="form-inline mt-2 mt-md-0">
    <input class="form-control mr-sm-2" type="text" placeholder="Search for ..." aria-label="Search for ...">
    <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
</form>