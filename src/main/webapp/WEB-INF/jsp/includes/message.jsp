<div class="alert alert-success alert-dismissible" role="alert" auto-close="5000" style="display: none" >
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h3>{{message}}</h3>
</div>