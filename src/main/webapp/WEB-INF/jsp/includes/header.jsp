<!doctype html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="includes.jsp"%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="fviewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<c:url value="/assests/image/logo.jpg"/>"/>
        <title>F1Shop - A Complete Shopping</title>   

        <link href="<c:url value="/assests/css/bootstrap.css" />" rel="stylesheet">
        <link href="<c:url value="/assests/css/carousel.css" />" rel="stylesheet"> 
        <link href="<c:url value="/assests/css/bootstrap-imageupload.css" />" rel="stylesheet">
        <!--<link href="<c:url value="/assests/css/font-awesome.min.css" />" rel="stylesheet">-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="<c:url value="/assests/js/jquery-3.3.1.slim.min.js"/>"></script>
        <script src="<c:url value="/assests/js/popper.min.js"/>"></script>
        <script src="<c:url value="/assests/js/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/assests/js/holder.min.js"/>"></script>
        <script src="<c:url value="/assests/js/offcanvas.js"/>"></script>
        <script src="<c:url value="/assests/js/bootstrap-imageupload.js"/>"></script>
        <script src="<c:url value="/assests/js/angular.min.js"/>"></script>
        <script src="<c:url value="/assests/js/cartController.js"/>"></script>

        <!--For Admin Panel-->
<!--        <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css">	
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <script src="https://use.fontawesome.com/07b0ce5d10.js"></script>-->
        <!--<link href="<c:url value="/assests/css/admin.css" />" rel="stylesheet">--> 


        <!------ Include the above in your HEAD tag ---------->

        <style>
            .checkbox-rounded [type="checkbox"][class*='filled-in']+label:after {
                border-radius: 50%;
            }
            .text-nowrap {
                white-space: nowrap;
            }

            .dropdown:hover .dropdown-menu {
                display: block;
                margin-top: 0;
            }
            .badge {
                padding: 1px 9px 2px;
                font-size: 12.025px;
                font-weight: bold;
                white-space: nowrap;
                color: #ffffff;
                background-color: #265a88;
                -webkit-border-radius: 9px;
                -moz-border-radius: 9px;
                border-radius: 9px;
            }

            .product_view .modal-dialog{max-width: 800px; width: 100%;}
            .pre-cost{text-decoration: line-through; color: #a5a5a5;}
            .space-ten{padding: 10px 0;}
            .thumbnail {
                overflow: hidden;margin-bottom: 20px;
            }
            .thumbnail img {width: 100%;}

            .product_img img {
                width: 100%;
                padding: 0 60px;
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">                
                <a class="navbar-brand" href="#"><link rel="icon" href="<c:url value="assests/image/logo.png"/>"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <!--                        <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" href="" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Settings</a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                                                        <a class="dropdown-item" href="<spring:url value="/admin/suppliers"/>">Supplier</a>
                                                        <a class="dropdown-item" href="">Product Category</a>
                                                        <a class="dropdown-item" href="#">Something else here</a>
                                                    </div>
                                                </li>-->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="ddMenuItemSelect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</a>
                            <div class="dropdown-menu" aria-labelledby="ddMenuItemSelect">                                
                                <c:if test="${not empty categoryList}">
                                    <c:forEach items="${categoryList}" var="category">
                                        <a class="dropdown-item" href="<spring:url value="/shopping/${category.id}"/>">${category.name}</a>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link disabled" href="<spring:url value="/admin/supplier/create"/>">All Products</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="<spring:url value="/admin/supplier/create"/>">Admin Panel</a>
                        </li>

                    </ul>
                    <ul class="navbar-nav pull-right">
                        <c:choose>
                            <c:when test="${pageContext.request.userPrincipal.name != null }" >
                                <li class="nav-item" style="color:greenyellow" >
                                    Welcome: ${pageContext.request.userPrincipal.name} ${currentUser.firstName}
                                </li>
                                <li class="nav-item" style="color:red" >
                                    |
                                </li> 
                                <li class="nav-item" style="color:yellow">
                                    <a href="<c:url value="/j_spring_security_logout"/>"> Logout</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<c:url value="/customer/cart"/>">
                                        <span class="badge">Cart: ${cart} (2)</span>
                                    </a>
                                </li>
                                <li>  
                                    <%@ include file="/WEB-INF/jsp/includes/search.jsp"%>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item">
                                    <a class="nav-link" href="<c:url value="/login"/>">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<c:url value="/register"/>">Register</a>
                                </li>
                                <li>    
                                    <%@ include file="/WEB-INF/jsp/includes/search.jsp"%>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </nav>
        </header>
        <hr>