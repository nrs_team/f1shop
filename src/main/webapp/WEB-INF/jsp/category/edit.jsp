<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Update Category: </h4> 
    <hr>
    <form:form action="${pageContext.request.contextPath}/admin/category/update/${category.id}" enctype = "multipart/form-data" method="POST" commandName="category">

        <div class="form-group row">
            <label for="categoryName" class="col-1 col-form-label">Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="name" id="categoryName"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-1 col-form-label">Description: </label>
            <div class="col-6">
               <form:input class="form-control" type="text" path="description" id="description"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="categoryImage" class="col-2 col-form-label text-nowrap">Upload Id/Photo:  </label>
            <div class="col-10">
                <form:input type = "file" name = "categoryImage" class="image-name btn"  path="categoryImage" id="categoryImage" size = "50"  />
            </div>
            <div class="col-2">
                <img id="thumb" src="<c:url value="/assests/image/category/${category.id}.png" />" alt="your image" height="100" />
            </div>
        </div>
        <div class="form-group row">
            <label for="submitBtn" class="col-2 col-form-label text-nowrap">Display:  </label>
            <div class="col-10">
                <input type="checkbox" path="isActive" name="isActive" ${category.isActive?"checked":""} data-toggle="toggle">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-1">
                <button type="submit" id="submitBtn" class="btn btn-danger">Update</button>
            </div>
            <div class="col-1">
                <a href="<c:url value="/admin/categories"/>" class="btn btn-primary">Cancel</a>
            </div>

        </div>

    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>