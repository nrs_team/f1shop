<%-- 
    Document   : view
    Created on : 28/04/2018, 11:26:21 AM
    Author     : NRS Team
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<main role="main">
    <div class="container-wrapper">
        <div class="container">
            <div class="page-hader">
                <h1>Category Details</h1>

            </div>

            <div class="container">

                <div class="row">
                    <div class="col-md-5">

                        <form:form action="${pageContext.request.contextPath}/admin/categories" >
                            <div class="form-group">
                                <label for="usr">Name:</label>
                                ${category.name}
                            </div>
                            <div class="form-group">
                                <label for="pwd">Description:</label>
                                ${category.description}
                            </div>
                            <div class="form-group">
                                <label>Image:</label>
                                <img id="thumb" src="<c:url value="/assests/image/category/${category.id}.png" />" alt="your image" height="100" />
                            </div>
                            <div class="checkbox">
                                <label> Status: 
                                    ${category.isActive?"checked":""}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="<spring:url value="/admin/categories"/>" class="btn btn-success" >Cancel</a>
                                    <a href="<spring:url value="/admin/category/edit/${product.id}"/>" class="btn btn-primary" >Edit</a>
                                    <a href="<spring:url value="/admin/category/delete/${product.id}"/>" class="btn btn-danger" >Delete</a>
                                </div>
                            </div>
                        </form:form>
                    </div>




                </div>
            </div>
        </div>



</main>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>