<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Product Details: </h4> 
    <form:form action="" method="POST" commandName="supplier">
        <div class="form-group row">
            <label for="productName" class="col-1 col-form-label text-nowrap">Name  <span class="fa fa-address-book">: </span></label>
            <div class="col-6">
                <label for="productName" class="col-1 col-form-label text-nowrap"> ${product.name}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="categoryName" class="col-1 col-form-label">Category <span class="fa fa-envelope">: </span> </label>
            <div class="col-6">
                <label for="categoryName" class="col-1 col-form-label text-nowrap"> ${product.category.name}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplierName" class="col-1 col-form-label">Supplier <span class="fa fa-mobile">: </span></label>
            <div class="col-6">
                <label for="supplierName" class="col-1 col-form-label text-nowrap"> ${product.supplier.name}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="brand" class="col-1 col-form-label text-nowrap">Brand <span class="fa fa-address-card">: </label>
            <div class="col-6">
                <label for="brand" class="col-1 col-form-label text-nowrap"> ${product.brand} </label>
            </div>
        </div>
        <div class="form-group row">
            <label for="unit_price" class="col-1 col-form-label text-nowrap">Unit Price <span class="fa fa-address-card">: </label>
            <div class="col-6">
                <label for="unit_price" class="col-1 col-form-label text-nowrap"> ${product.unitPrice} </label>
            </div>
        </div>
        <div class="form-group row">
            <label for="quantity" class="col-1 col-form-label text-nowrap">Quantity <span class="fa fa-address-card">: </label>
            <div class="col-6">
                <label for="quantity" class="col-1 col-form-label text-nowrap"> ${product.quantity} </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="image" class="col-1 col-form-label">Image: </label>
            <div class="col-6">
                <img id="thumb" src="<c:url value="/assests/image/product/${product.id}.png" />" alt="your image" height="100" />
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<spring:url value="/admin/products"/>" class="btn btn-success" >Cancel</a>
                <a href="<spring:url value="/admin/product/edit/${product.id}"/>" class="btn btn-primary" >Edit</a>
                <a href="<spring:url value="/admin/product/delete/${product.id}"/>" class="btn btn-danger" >Delete</a>
            </div>
        </div>
    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>