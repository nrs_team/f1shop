<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <div class="col-md-12">
        <h4>Products</h4>
        <hr>
        <form:form class="form-inline mt-2 mt-md-0" action="${pageContext.request.contextPath}/admin/product/search" method="POST">
            <div class="row">

                <div class="col-sm-12">
                    <input class="form-control col-sm-6" type="text" placeholder="Search" aria-label="Search" 
                           id="searchedSupplierName" name="searchedProductName"/>
                    <button class="btn btn-outline-danger my-2 my-sm-4" type="submit">Search</button>
                    <a href="<spring:url value="product/add"/>" class="btn btn-primary" >Create New Product</a>
                </div>
            </div>
        </form:form>

        <div class="table-responsive">
            <table id="mytable" class="table table-bordred table-striped">
                <thead>
                <th><input type="checkbox" id="checkall" /></th>
                <th>Product Name</th>
                <th>Category</th>
                <th>Supplier</th>
                <th>Image</th>
                <th></th>
                <th></th>
                </thead>
                <tbody>
                    <c:forEach items="${productList}" var="product">
                        <tr>
                            <td><input type="checkbox" class="checkthis" /></td>
                            <td>${product.name}</td>
                            <td>${product.category.name}</td>
                            <!--problem is here-->
                            <td>${product.supplier.name}</td> 
                            <td>
                                <img id="blah" src="<c:url value="/assests/image/product/${product.id}.png" />" alt="your image" height="100" />
                            </td>
                            <td class="btn-group mr-2">

                                <a href="<spring:url value="product/view/${product.id}" />"> 
                                    <p data-placement="top" data-toggle="tooltip" title="Detail">
                                        <button class="btn btn-success btn-xs" data-title="Detail" data-toggle="modal" data-target="#Detail" >
                                            <span class="fa fa-info"></span>
                                        </button>
                                    </p>
                                </a>

                                <a href="<spring:url value="product/edit/${product.id}" />"> 
                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                        <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" >
                                            <span class="fa fa-pencil"></span>
                                        </button>
                                    </p>
                                </a>
                                <a href="<spring:url value="product/delete/${product.id}" />">
                                    <p data-placement="top" data-toggle="tooltip" title="Delete">
                                        <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" >
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </p>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
       
        <%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>