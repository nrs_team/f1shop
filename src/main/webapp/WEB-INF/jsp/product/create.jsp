<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Create new Product: </h4>
    <hr>
    <form:form action="${pageContext.request.contextPath}/admin/product/create" enctype="multipart/form-data" method="POST" commandName="product">

        <div class="form-group row">
            <label for="productName" class="col-1 col-form-label">Name:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="name" id="productName"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="category" class="col-1 col-form-label">Category: </label>
            <div class="col-6">
               <form:select path="categoryId">
                   <form:option value="" label="--- Select Category---" />
                   <form:options items="${categoryList}" itemValue="id" itemLabel="name"/>
               </form:select>
            </div>
        </div>
        <div class="form-group row">
            <label for="supplier" class="col-1 col-form-label">Supplier </label>
            <div class="col-6">
               <form:select path="supplierId">
                   <form:option value="" label="--- Select Supplier---" />
                   <form:options items="${supplierList}" itemValue="id" itemLabel="name"/>
               </form:select>
            </div>
        </div>
        <div class="form-group row">
            <label for="brandName" class="col-1 col-form-label">Brand:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="brand" id="brandName"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="remarks" class="col-1 col-form-label">Description:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="description" id="remarks"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="unit_price" class="col-1 col-form-label">Unit Price:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="unitPrice" id="unit_price"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="quantity_number" class="col-1 col-form-label">Quantity:</label>
            <div class="col-6">
                <form:input class="form-control" type="text" path="quantity" id="quantity_number"/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 col-form-label text-nowrap">Image Url:  </label>
            <div class="col-10">
                <input type = "file" name = "productImage" path="productImage" class="image-name btn" size = "50"  />
            </div>
            <div class="col-2">
                <img id="thumb" src="../../../assests/image/rectangle.png" alt="your image" height="100" />
            </div>
        </div>
        <div class="form-group row">
            <div class="col-1">
                <button type="submit" id="submitBtn" class="btn btn-danger">Create</button>
            </div>
            <div class="col-1">
                <a href="<c:url value="/admin/products"/>" class="btn btn-primary">Cancel</a>
            </div>

        </div>
    </form:form>
</div>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>