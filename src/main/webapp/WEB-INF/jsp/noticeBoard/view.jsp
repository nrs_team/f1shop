<%-- 
    Document   : view
    Created on : 28/04/2018, 11:26:21 AM
    Author     : NRS Team
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<main role="main">
    <div class="container-wrapper">
        <div class="container">
            <div class="page-hader">
                <h1>Notice Board Details</h1>

            </div>

            <div class="container">

                <div class="row">
                    <div class="col-md-5">

                        <form:form action="${pageContext.request.contextPath}/admin/noticeBoards" >
                            <div class="form-group">
                                <label for="usr">Title:</label>
                                ${noticeBoard.title}
                            </div>
                            <div class="form-group">
                                <label for="pwd">Description:</label>
                                ${noticeBoard.description}
                            </div>
                            <div class="form-group">
                                <label>Image:</label>
                                <img id="thumb" src="<c:url value="/assests/image/noticeBoard/${noticeBoard.id}.png" />" alt="your image" height="100" />
                            </div>
                            <div class="checkbox">
                                <label> Status: 
                                    ${noticeBoard.status?"checked":""}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="<spring:url value="/admin/noticeBoards"/>" class="btn btn-success" >Cancel</a>
                                    <a href="<spring:url value="/admin/noticeBoard/edit/${noticeBoard.id}"/>" class="btn btn-primary" >Edit</a>
                                    <a href="<spring:url value="/admin/noticeBoard/delete/${noticeBoard.id}"/>" class="btn btn-danger" >Delete</a>
                                </div>
                            </div>
                        </form:form>
                    </div>




                </div>
            </div>
        </div>



</main>
<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>