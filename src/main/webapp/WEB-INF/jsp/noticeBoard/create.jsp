<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
<div class="container">
    <h4>Create new category: </h4>
    <hr>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <form:form action="${pageContext.request.contextPath}/admin/noticeBoard/create" enctype = "multipart/form-data" method="POST" commandName="noticeBoard"  >

        <div class="form-group row">
            <label for="noticeBoardTitle" class="col-1 col-form-label">Title:</label>
            <div class="col-6">
                <form:input class="form-control"  type="text" path="noticeBoardTitle" id="noticeBoardTitle"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-1 col-form-label">Description: </label>
            <div class="col-6">
               <form:input class="form-control" name="description" type="text" path="description" id="description"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="noticeBoardImage" class="col-2 col-form-label text-nowrap">Image:  </label>
            <div class="col-10">
                <form:input type = "file" name = "noticeBoardImage" class="image-name btn"  path="noticeBoardImage" id="noticeBoardImage" size = "50" />
            </div>
            <div class="col-2">
                <img id="thumb" src="../../../assests/image/rectangle.png" alt="your image" height="100" />
            </div>
        </div>
        <div class="form-group row">
            <label for="isActive" class="col-2 col-form-label text-nowrap">Display:  </label>
            <div class="col-10">
                <input type="checkbox" path="isActive" name="isActive" checked data-toggle="toggle"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-1">
                <button type="submit" id="submitBtn" class="btn btn-danger">Create</button>
            </div>
            <div class="col-1">
                <a href="<c:url value="/admin/noticeBoards"/>" class="btn btn-primary">Cancel</a>
            </div>

        </div>
    </form:form>
</div>


<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>