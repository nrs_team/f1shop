<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes/header.jsp"%>
        <div class="col-md-12">
            <h4>Categories</h4>
            <form:form class="form-inline mt-2 mt-md-0" action="${pageContext.request.contextPath}/admin/noticeBoard/search" method="POST">
                <div class="container">
                    <div class="row">

                        <div class="col-md-6">

                            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" name="name" path="name"/>
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>

                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<spring:url value="/admin/addNoticeBoard" ></spring:url>" class="btn btn-primary pull-right" >Create New Notice Board</a>
                            </div>


                        </div>
                    </div>
            </form:form>

            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <th><input type="checkbox" id="checkall" /></th>
                    <th>Title</th>
                    <th>Description</th>
                    <!--<th>Image</th>-->
                    <th>Status</th>
                    <th>Action</th>

                    </thead>
                    <tbody>
                        <c:forEach items="${noticeBoardList}" var="noticeBoard">
                            <tr>
                                <td><input type="checkbox" class="checkthis" /></td>
                                <td>
                                    <a href="<spring:url value="viewNoticeBoard/${noticeBoard.id}" />">${noticeBoard.title}</a>
                                </td>
                                <td>${noticeBoard.description}</td>
<!--                                <td>
                                    <img id="thumb" src="<c:url value="/assests/image/noticeBoard/${noticeBoard.id}.png" />" alt="your image" height="100" />
                                </td>-->
                                <td>Available</td>
                                <td class="btn btn-group">

                                    <a href="<spring:url value="/admin/viewNoticeBoard/${noticeBoard.id}" />"> 
                                        <p data-placement="top" data-toggle="tooltip" title="Detail">
                                            <button class="btn btn-success btn-xs" data-title="Detail" data-toggle="modal" data-target="#Detail" >
                                                <span class="fa fa-info"></span>
                                            </button>
                                        </p>
                                    </a>
                                    <a href="<spring:url value="/admin/editNoticeBoard/${noticeBoard.id}" />">
                                        <p data-placement="top" data-toggle="tooltip" title="Edit">
                                            <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" >
                                                <span class="fa fa-pencil"></span>
                                            </button>
                                        </p>
                                    </a>
                                    <a href="<spring:url value="/admin/deleteNoticeBoard/${noticeBoard.id}" />">
                                        <p data-placement="top" data-toggle="tooltip" title="Delete">
                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" >
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </p>
                                    </a>

                                </td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                
        </div>
    


<%@ include file="/WEB-INF/jsp/includes/footer.jsp"%>
