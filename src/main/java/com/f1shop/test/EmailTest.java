/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.test;

import com.f1shop.service.impl.Mail;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Lenovo
 */
public class EmailTest {

    @SuppressWarnings("resource")
    public static void main(String args[]) {

        // Spring Bean file you specified in /src/main/resources folder
        String crunchifyConfFile = "Spring-Mail.xml";
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(crunchifyConfFile);

        // @Service("crunchifyEmail") <-- same annotation you specified in CrunchifyEmailAPI.java
        Mail crunchifyEmailAPI = (Mail) context.getBean("mail");
        String toAddr = "test@crunchify.com";
        String fromAddr = "test@crunchify.com";

        // email subject
        String subject = "Hey.. This email sent by Crunchify's Spring MVC Tutorial";

        // email body
        String body = "There you go.. You got an email.. Let's understand details on how Spring MVC works -- By Crunchify Admin";
        crunchifyEmailAPI.sendMail(fromAddr,toAddr, subject, body);
    }

}
