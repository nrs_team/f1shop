/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.model.pojo;

import com.f1shop.model.UserData;
import com.f1shop.model.Wallet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class Cart {

    private String cartId;
    private Map<String, CartItem> cartItems;
    private Double grandTotal;
    private Integer totalItemCount;
    private Wallet customerWallet;
    private UserData userData; 


    public Cart() {
        cartItems = new HashMap<String, CartItem>();
        grandTotal = 0.0;
    }

    public Cart(String cartId) {
        this();
        this.cartId = cartId;
    }
    public Cart(String cartId, UserData userData, Wallet wallet) {
        this();
        this.cartId = cartId;
        this.cartId = cartId;
        this.userData= userData; 
    }
    /**
     * @return the cartId
     */
    public String getCartId() {
        return cartId;
    }

    /**
     * @param cartId the cartId to set
     */
    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    /**
     * @return the cartItems
     */
    public Map<String, CartItem> getCartItems() {
        return cartItems;
    }

    /**
     * @param cartItems the cartItems to set
     */
    public void setCartItems(Map<String, CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    /**
     * @return the grandTotal
     */
    public Double getGrandTotal() {
        return grandTotal;
    }

    /**
     * @param grandTotal the grandTotal to set
     */
    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public void addCartItem(CartItem item) {
        String productId = item.getProduct().getId().toString();
        if (cartItems.containsKey(productId)) {
            CartItem existingCart = cartItems.get(productId);
            existingCart.setQuantity(existingCart.getQuantity() + 1);
            existingCart.setTotalPrice(existingCart.getQuantity() * item.getTotalPrice());
            cartItems.put(productId, existingCart);

        } else {
            cartItems.put(productId, item);
        }
        evaluateTotal();
    }

    public void removeCartItem(CartItem item) {
        String productId = item.getProduct().getId().toString();
        cartItems.remove(productId);
        evaluateTotal();
    }

    public void reduceCartItem(CartItem item) {
        String productId = item.getProduct().getId().toString();
        if (cartItems.containsKey(productId)) {
            CartItem existingCart = cartItems.get(productId);
            existingCart.setQuantity(existingCart.getQuantity() - 1);
            existingCart.setTotalPrice(existingCart.getQuantity() * item.getTotalPrice());
            if (existingCart.getQuantity() == 0) {
                removeCartItem(item);
            } else {
                cartItems.put(productId, existingCart);
                evaluateTotal();
            }

        }

    }

    private void evaluateTotal() {
        grandTotal = 0.0;
        totalItemCount = 0;
        for (CartItem item : cartItems.values()) {
            grandTotal = grandTotal + item.getTotalPrice();
            totalItemCount = totalItemCount + item.getQuantity();
        }

    }

    /**
     * @return the totalItemCount
     */
    public int getTotalItemCount() {
        return totalItemCount;
    }

    /**
     * @param totalItemCount the totalItemCount to set
     */
    public void setTotalItemCount(int totalItemCount) {
        this.totalItemCount = totalItemCount;
    }
    /**
     * @return the userData
     */
    public UserData getUserData() {
        return userData;
    }

    /**
     * @param userData the userData to set
     */
    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    /**
     * @return the customerWallet
     */
    public Wallet getCustomerWallet() {
        return customerWallet;
    }

    /**
     * @param customerWallet the customerWallet to set
     */
    public void setCustomerWallet(Wallet customerWallet) {
        this.customerWallet = customerWallet;
    }


}
