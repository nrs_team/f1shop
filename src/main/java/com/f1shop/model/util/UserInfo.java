package com.f1shop.model.util;

import com.f1shop.service.impl.UserDataServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import com.f1shop.model.UserData;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Sunil
 */
public class UserInfo {

    @Autowired
    UserDataServiceImpl userservice;

    public UserData getCurrentUser() {

        UserData userData = new UserData();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();

            if (currentUserName == null) {

                Authentication auth = SecurityContextHolder.getContext().getAuthentication();

                String name = auth.getName();

                userData = (UserData) userservice.getUserByUserName(name);

            }
        }

        return userData;

    }

}
