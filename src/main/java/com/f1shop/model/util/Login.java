package com.f1shop.model.util;

import com.f1shop.model.Customer;
import com.f1shop.model.UserData;
import com.f1shop.service.impl.UserDataServiceImpl;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Sunil
 */
public class Login {

    final public static String NEW_ACCOUNT_EMAIL_SUBJECT = "Your new account has been created.";
    final public static String NEW_ACCOUNT_EMAIL_MESSAGE = "Congraluation! Your new account has been sucessfully created. Now, you can login with your username and password.\n Thank you,\n NRS TEAM";
    final public static String ROLE_USER = "ROLE_USER";
    final public static String ROLE_ADMIN = "ROLE_ADMIN";

    public boolean signup(UserData userData) {

        try {
            String encodedPassword = encodePasswordWithBCrypt(userData.getPassword());
            System.out.println("Encoded Password: " + encodedPassword);
            userData.setPassword(encodedPassword);
            userData.setIsActive(Boolean.TRUE);

            /*
            Due to security reason, admin users will be created by admin manually 
             */
            userData.setAuthority(ROLE_USER);

            /*
            Setting default customer attributes
            */
            Customer customer = new Customer();
            customer.setIsActive(Boolean.TRUE);
            customer.setEmailId(userData.getCustomer().getEmailId());
            
            userData.setCustomer(customer);
            userData.getCustomer().setCreatedDate(new Date());
            
            
            

        } catch (Exception ex) {
            return false;
        }

        return true;

    }

    public Boolean isUserAuthenticated(UserDataServiceImpl userService, UserData user) {

        Boolean isAuthenticated = false;

        UserData userFromDB = userService.getUser(user.getId());

        isAuthenticated = new BCryptPasswordEncoder().matches(user.getPassword(), userFromDB.getPassword());

        return isAuthenticated;
    }

    public static String generateHash(String input) {
        StringBuilder hash = new StringBuilder();

        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(input.getBytes());
            char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
            for (int idx = 0; idx < hashedBytes.length; ++idx) {
                byte b = hashedBytes[idx];
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {
            // handle error here.
        }

        return hash.toString();
    }

    public static String encodePasswordWithBCrypt(String plainPassword) {
        return new BCryptPasswordEncoder().encode(plainPassword);
    }

}
