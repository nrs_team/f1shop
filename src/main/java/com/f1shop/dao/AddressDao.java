/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;

import com.f1shop.model.Address;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface AddressDao {

    public void add(Address address);

    public void edit(Address address);

    public void delete(Address address);

    public Address getAddress(Long addressId);

    public List getAllAddress();

    public List getSearchedAddresses(String like);
}
