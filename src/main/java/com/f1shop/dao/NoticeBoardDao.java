/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;

import com.f1shop.model.NoticeBoard;
import java.util.List;

/**
 *
 * @author Rayan
 */
public interface NoticeBoardDao {
    public void add(NoticeBoard noticeBoard);
    
    public List getAllNoticeBoard();
    
    public void delete(NoticeBoard noticeBoard);
    
    public NoticeBoard getNoticeBoard(Long noticeBoardId);
    
    public void update(NoticeBoard noticeBoard);
    
    public List getSearchedNoticeBoards(String like);
}
