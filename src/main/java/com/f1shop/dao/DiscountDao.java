/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;

import com.f1shop.model.Discount;
import java.util.List;

/**
 *
 * @author Sunil Tako
 */
public interface DiscountDao {

    public void add(Discount discount);

    public void edit(Discount discount);

    public void delete(Discount discount);

    public Discount getDiscount(Long discountId);

    public List getAllDiscount();

    public List getSearchedDiscounts(String like);

}
