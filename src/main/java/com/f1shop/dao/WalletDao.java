/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;

import com.f1shop.model.Wallet;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface WalletDao {

    public void add(Wallet wallet);

    public void edit(Wallet wallet);

    public void delete(Wallet wallet);

    public Wallet getWallet(Long walletId);

    public List<Wallet> getAllWallet();

    public List getAllSearchedCustomerWallet(String like);

    public Wallet getCustomerWallet(Long customerId);
}
