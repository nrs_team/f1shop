/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;

import com.f1shop.model.pojo.Cart;

/**
 *
 * @author Admin
 */

public interface CartDao {
    Cart create(Cart cart); 
    Cart read(String cartId); 
    void update(String cartId, Cart cart); 
    void delete(String cartId); 
}
