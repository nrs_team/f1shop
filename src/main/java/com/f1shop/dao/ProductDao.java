/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;

import com.f1shop.model.Product;
import java.util.List;

/**
 *
 * @author Rayan
 */
public interface ProductDao {
    public List getAllProduct();

    public void add(Product product);

    public Product getProduct(Long productId);

    public void delete(Product product);

    public List getSearchedProducts(String like);

    public List<Product> getSearchedProductsByCategory(String searchedProductName, Long categoryId);

    public List getListProductsByCategory(Long categoryId);
}
