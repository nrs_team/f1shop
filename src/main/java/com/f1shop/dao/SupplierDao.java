package com.f1shop.dao;

import com.f1shop.model.Supplier;
import java.util.List;

/**
 *
 * @author NRS Team
 * @date 2018-04-21
 */
public interface SupplierDao {

    public void add(Supplier supplier);

    public void edit(Supplier supplier);

    public void delete(Supplier supplier);

    public Supplier getSupplier(Long supplierId);

    public List<Supplier> getAllSupplier();

    public List getAllSearchedSupplier(String like);
}
