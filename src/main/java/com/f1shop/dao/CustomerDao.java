package com.f1shop.dao;

import com.f1shop.model.Customer;
import java.util.List;

public interface CustomerDao {

    public void add(Customer customer);
    public void edit(Customer customer);
    public void delete(Customer customer);
    public Customer getCustomer(Long customerId);
    public List getAllCustomer();

}
