/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;

import com.f1shop.model.UserData;
import java.util.List;

/**
 *
 * @author Sunil
 */
public interface UserDataDao {
    
    public void add(UserData user);
    public void edit(UserData user);
    public void delete(UserData user);
    public UserData getUser(Long userId);
    public List getAllUsers();
    public UserData getUserByUserName(String username);
    
}
