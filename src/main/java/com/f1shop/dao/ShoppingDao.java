package com.f1shop.dao;

import com.f1shop.model.OrderDetail;
import com.f1shop.model.OrderItem;
import com.f1shop.model.Product;
import com.f1shop.model.UserData;
import com.f1shop.model.Wallet;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface ShoppingDao {

    //update the available quantity in the inventory
    public void updateInventory(List<Product> products);

    //update the balance in the wallet
    public void ProcessPayment(Wallet wallet);

    //update the order item record
    public void addOrderItem(OrderItem items);

    //update the order details
    public Long addOrderDetail(OrderDetail orderDetail);

    public List getAllOrderDetail();
}
