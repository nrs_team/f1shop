/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao;
import com.f1shop.model.Category;
import java.util.List;
/**
 *
 * @author Rayan
 */
public interface CategoryDao {
    
    public void add(Category category);
    
    public List getAllCategory();
    
    public void delete(Category category);
    
    public Category getCategory(Long categoryId);
    
    public void update(Category category);
    
    public List getSearchedCategories(String like);
}
