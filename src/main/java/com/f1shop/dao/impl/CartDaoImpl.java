package com.f1shop.dao.impl;

import com.f1shop.dao.CartDao;
import com.f1shop.dao.WalletDao;
import com.f1shop.model.Wallet;
import com.f1shop.model.pojo.Cart;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author NRS Team
 * @date 08/05/2018
 */
@Repository
public class CartDaoImpl implements CartDao {

    @Autowired
    private WalletDao walletDao;
    final String CustomerWalletID ="1";
    private Map<String, Cart> cartList;

    public CartDaoImpl() {
        cartList = new HashMap<>();
    }

    @Override
    public Cart create(Cart cart) {
        if (cartList.keySet().contains(cart.getCartId())) {
            throw new IllegalArgumentException(String.format("Can't create cart. Cart {0} already exists", cart.getCartId()));
        }
        cartList.put(cart.getCartId(), cart);
       
        return cart;
    }

    @Override
    public Cart read(String cartId) {
        return cartList.get(cartId);
    }

    @Override
    public void update(String cartId, Cart cart) {
        if (!cartList.keySet().contains(cartId)) {
            throw new IllegalArgumentException(String.format("Unable to update, Cart {0} doesn't exists", cart.getCartId()));
        }       
        cartList.put(cartId, cart);

    }

    @Override
    public void delete(String cartId) {
        if (!cartList.keySet().contains(cartId)) {
            throw new IllegalArgumentException(String.format("Unable to delete, Cart {0} doesn't exists", cartId));
        }
        cartList.remove(cartId);
    }

}
