/*
@Author : NRS Team
@Date: 2018-04-21
@objective: All the actions related to supplier (insert/update/delete/search) 
 */
package com.f1shop.dao.impl;

import com.f1shop.dao.SupplierDao;
import com.f1shop.model.Supplier;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SupplierDaoImpl implements SupplierDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(Supplier supplier) {
        session.getCurrentSession().save(supplier);
    }

    @Override
    public void edit(Supplier supplier) {
        session.getCurrentSession().update(supplier);
    }

    @Override
    public void delete(Supplier supplier) {
        session.getCurrentSession().delete(supplier);
    }

    @Override
    public Supplier getSupplier(Long supplierId) {
        return (Supplier) session.getCurrentSession().get(Supplier.class, supplierId);
    }

    @Override
    public List<Supplier> getAllSupplier() {
        return session.getCurrentSession().createQuery("from Supplier WHERE isActive= true").list();
    }

    @Override
    public List getAllSearchedSupplier(String like) {
        Query query = session.getCurrentSession().createQuery("FROM Supplier s WHERE lower(s.name) LIKE lower(:like) and s.isActive= true");
        query.setParameter("like", "%" + like + "%");
        return query.list();
    }

}
