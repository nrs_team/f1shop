package com.f1shop.dao.impl;

import org.springframework.stereotype.Repository;
import com.f1shop.dao.CategoryDao;
import com.f1shop.model.Category;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author NRS Tea
 */

@Repository
public class CategoryDaoImpl implements CategoryDao {
    
    @Autowired
    private SessionFactory session;
    
    @Override
    public void add(Category category){
        session.getCurrentSession().save(category);
    }
    
    public List getAllCategory(){
        return session.getCurrentSession().createQuery("from Category where isActive = TRUE ORDER BY created_date DESC").list();
    }
    
    public Category getCategory(Long categoryId){
        return (Category) session.getCurrentSession().get(Category.class, categoryId);
    }
    
    @Override
    public void delete(Category category){
        session.getCurrentSession().update(category);
    }
    
    @Override
    public void update(Category category){
        session.getCurrentSession().update(category);
    }
    
    @Override
    public List getSearchedCategories(String like){
        Query query = session.getCurrentSession().createQuery("FROM Category c WHERE lower(c.name) LIKE lower(:like) AND isActive = TRUE");
        query.setParameter("like", "%" + like + "%");        
        return query.list();
    }
}
