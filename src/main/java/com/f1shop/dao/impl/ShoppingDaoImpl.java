package com.f1shop.dao.impl;

import com.f1shop.dao.ShoppingDao;
import com.f1shop.model.OrderDetail;
import com.f1shop.model.OrderItem;
import com.f1shop.model.Product;
import com.f1shop.model.Wallet;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Admin
 */
@Repository
public class ShoppingDaoImpl implements ShoppingDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void updateInventory(List<Product> products) {
        session.getCurrentSession().update(products);
    }

    @Override
    public void ProcessPayment(Wallet wallet) {
        session.getCurrentSession().update(wallet);
    }

    @Override
    public void addOrderItem(OrderItem item) {
            session.getCurrentSession().save(item);
    }

    @Override
    public Long addOrderDetail(OrderDetail orderDetail) {
        session.getCurrentSession().save(orderDetail);
        Long result = orderDetail.getId();
        return result;
    }

    @Override
    public List getAllOrderDetail() {
        return session.getCurrentSession().createQuery("from OrderItem where status = 1").list();
       
    }

}
