package com.f1shop.dao.impl;

import com.f1shop.dao.ProductDao;
import com.f1shop.model.Product;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private SessionFactory session;

    //get the all product in the list
    public List getAllProduct() {
        return session.getCurrentSession().createQuery("from Product where isActive = 1").list();
    }

    public void add(Product product) {
        session.getCurrentSession().save(product);
    }

    public Product getProduct(Long productId) {
        return (Product) session.getCurrentSession().get(Product.class, productId);
    }

    @Override
    public void delete(Product product) {
        session.getCurrentSession().update(product);
    }

    @Override
    public List getSearchedProducts(String like) {
        Query query = session.getCurrentSession().createQuery("FROM Product c WHERE lower(c.name) LIKE lower(:like) AND isActive = TRUE");
        query.setParameter("like", "%" + like + "%");
        return query.list();
    }
    
    @Override
    public List getListProductsByCategory(Long categoryId){
        Query query = session.getCurrentSession().createQuery("FROM Product c WHERE isActive = TRUE AND category_id = " + categoryId);
        return query.list();
    }

    @Override
    public List<Product> getSearchedProductsByCategory(String searchedProductName, Long categoryId) {
        Query query = session.getCurrentSession().createQuery("FROM Product c WHERE lower(c.name) LIKE lower(:like) AND isActive = TRUE AND categoryId = " + categoryId);
        query.setParameter("like", "%" + searchedProductName + "%");
        return query.list();
    }
}
