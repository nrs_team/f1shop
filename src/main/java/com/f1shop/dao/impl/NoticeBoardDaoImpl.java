/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao.impl;

import com.f1shop.dao.NoticeBoardDao;
import com.f1shop.model.NoticeBoard;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NoticeBoardDaoImpl implements NoticeBoardDao {
    
    @Autowired
    private SessionFactory session;
    
    public void add(NoticeBoard noticeboard){
        session.getCurrentSession().save(noticeboard);
    }
    
    public List getAllNoticeBoard(){
        return session.getCurrentSession().createQuery("from NoticeBoard where status = TRUE ORDER BY createdDate DESC").list();
    }
    
    public NoticeBoard getNoticeBoard(Long noticeBoardId){
        return (NoticeBoard) session.getCurrentSession().get(NoticeBoard.class, noticeBoardId);
    }
    
    public void delete(NoticeBoard noticeBoard){
        session.getCurrentSession().update(noticeBoard);
    }
    
    public void update(NoticeBoard noticeBoard){
        session.getCurrentSession().update(noticeBoard);
    }
    
    public List getSearchedNoticeBoards(String like){
        Query query = session.getCurrentSession().createQuery("FROM NoticeBoard c WHERE lower(c.title) LIKE lower(:like) AND remarks = TRUE");
        query.setParameter("like", "%" + like + "%");        
        return query.list();
    }
}
