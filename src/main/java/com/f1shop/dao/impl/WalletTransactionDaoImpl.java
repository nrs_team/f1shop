
package com.f1shop.dao.impl;

import com.f1shop.dao.WalletTransactionDao;
import com.f1shop.model.WalletTransaction;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WalletTransactionDaoImpl implements WalletTransactionDao{
    @Autowired
    private SessionFactory session;
    public void add(WalletTransaction walletTransaction) {
        session.getCurrentSession().save(walletTransaction);
    }
}
