package com.f1shop.dao.impl;

import com.f1shop.dao.AddressDao;
import com.f1shop.model.Address;
import com.f1shop.model.Supplier;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AddressDaoImpl implements AddressDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(Address address) {
        session.getCurrentSession().save(address);
    }

    @Override
    public void edit(Address address) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Address address) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Address getAddress(Long addressId) {
       return (Address) session.getCurrentSession().get(Supplier.class, addressId);
    }
    @Override
    public List getAllAddress() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List getSearchedAddresses(String like) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
