/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao.impl;

import com.f1shop.dao.DiscountDao;
import com.f1shop.model.Discount;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sunil Tako
 */
@Repository
public class DiscountDaoImpl implements DiscountDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(Discount discount) {

        session.getCurrentSession().save(discount);
    }

    @Override
    public void edit(Discount discount) {
        session.getCurrentSession().update(discount);
    }

    @Override
    public void delete(Discount discount) {
        session.getCurrentSession().delete(discount);
    }

    @Override
    public Discount getDiscount(Long discountId) {
        return (Discount) session.getCurrentSession().get(Discount.class, discountId);
    }

    @Override
    public List getAllDiscount() {
        return session.getCurrentSession().createQuery("from Discount").list();
    }

    @Override
    public List getSearchedDiscounts(String like) {
        Query query = session.getCurrentSession().createQuery("FROM Discount d WHERE lower(d.name) LIKE lower(:like)");
        query.setParameter("like", "%" + like + "%");
        
        return query.list();
    }

}
