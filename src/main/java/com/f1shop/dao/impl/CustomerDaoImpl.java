package com.f1shop.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.f1shop.dao.CustomerDao;
import com.f1shop.model.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {
	
	@Autowired
	private SessionFactory session;

	@Override
	public void add( Customer customer ) {

		session.getCurrentSession( ).save( customer );
	}

	@Override
	public void edit( Customer customer ) {
		session.getCurrentSession( ).update( customer );
	}

	@Override
	public void delete( Customer customer ) {
		session.getCurrentSession( ).delete( customer );
	}

	@Override
	public Customer getCustomer( Long customerId ) {
		return (Customer)session.getCurrentSession( ).get( Customer.class, customerId );
	}

	@Override
	public List getAllCustomer( ) {
		return session.getCurrentSession( ).createQuery( "from Customer" ).list( );
	}

}
