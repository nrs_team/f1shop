/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao.impl;

import com.f1shop.model.UserData;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.f1shop.dao.UserDataDao;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.impl.CriteriaImpl;

/**
 *
 * @author Sunil
 */
@Repository
public class UserDataDaoImpl implements UserDataDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(UserData user) {

        session.getCurrentSession().save(user);
    }

    @Override
    public void edit(UserData user) {
        session.getCurrentSession().update(user);
    }

    @Override
    public void delete(UserData user) {
        session.getCurrentSession().delete(user);
    }

    @Override
    public UserData getUser(Long userId) {
       
       UserData obj = (UserData) session.getCurrentSession().get(UserData.class, userId);
        return obj;
    }

    @Override
    public List getAllUsers() {
        return session.getCurrentSession().createQuery("from UserData").list();
    }
    
    @Override
    public UserData getUserByUserName(String username) {
        List<UserData> users = session.getCurrentSession().createQuery("from UserData u where u.userName = :myUsername").setString("myUsername", username).list(); 
        return users.get(0); 
    }

}
