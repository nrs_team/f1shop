/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.dao.impl;

import com.f1shop.dao.WalletDao;
import com.f1shop.model.UserData;
import com.f1shop.model.Wallet;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Admin
 */
@Repository
public class WalletDaoImpl implements WalletDao {

    @Autowired
    private SessionFactory session;

    @Override
    public void add(Wallet wallet) {
        session.getCurrentSession().save(wallet);
    }

    @Override
    public void edit(Wallet wallet) {
        session.getCurrentSession().update(wallet);

    }

    @Override
    public void delete(Wallet wallet) {
        session.getCurrentSession().delete(wallet);
    }

    @Override
    public Wallet getWallet(Long walletId) {
        Wallet obj = (Wallet) session.getCurrentSession().get(Wallet.class, walletId);
        return (Wallet) session.getCurrentSession().get(Wallet.class, walletId);

    }

    @Override
    public List<Wallet> getAllWallet() {

        Query query = session.getCurrentSession().createQuery("from Wallet WHERE isActive= true");
        return query.list();
    }

    @Override
    public List getAllSearchedCustomerWallet(String like) {

//         Query query = session.getCurrentSession().createQuery("FROM Wallet s WHERE lower(s.name) LIKE lower(:like) and s.isActive= true");
//        query.setParameter("like", "%" + like + "%");
//        return query.list();
        return null;
    }

    @Override
    public Wallet getCustomerWallet(Long customerId) {
        List<Wallet> obj = session.getCurrentSession().createQuery("from Wallet where customerId=:customerId").setParameter("customerId", customerId).list();

        if (obj.size() > 0) {
            return obj.get(0);
        } else {
            return null;
        }
    }

}
