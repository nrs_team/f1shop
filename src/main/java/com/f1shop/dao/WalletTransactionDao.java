
package com.f1shop.dao;

import com.f1shop.model.WalletTransaction;

public interface WalletTransactionDao {
    public void add(WalletTransaction walletTransaction);
}
