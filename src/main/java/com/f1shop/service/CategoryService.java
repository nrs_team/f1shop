package com.f1shop.service;

import com.f1shop.model.Category;
import java.util.List;
/**
 *
 * @author NRS Team
 */
public interface CategoryService {
    
    public void add(Category category);
    
    public List getAllCategory();
    
    public Category getCategory(Long categoryId);
    
    public void delete(Category category);
    
    public void update(Category category);
    
    public List getSearchedCategories(String like);
}
