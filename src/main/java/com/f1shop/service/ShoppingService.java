/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.service;

import com.f1shop.model.OrderDetail;
import com.f1shop.model.OrderItem;
import com.f1shop.model.Product;
import com.f1shop.model.Wallet;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface ShoppingService {

    //update the product inventory (available quantity) in the inventory
    public void updateInventory(List<Product> product);

    //update the balance in the wallet
    public void ProcessPayment(Wallet wallet);

    //update the order item record
    public void addOrderItem(List<OrderItem> orderItem);

    //update the order details
    public Long addOrderDetail(OrderDetail orderDetail);

    public List getAllOrderDetail();

}
