package com.f1shop.service;

import com.f1shop.model.Wallet;
import java.util.List;

/*
@Author : NRS Team
@Date: 2018-05-21
@objective: All the actions related to customer wallet (insert/update/delete/search) 
 */
public interface WalletService {

    public void add(Wallet wallet);

    public void edit(Wallet wallet);

    public void delete(Wallet wallet);

    public Wallet getWallet(Long walletId);

    public List<Wallet> getAllWallet();

    public List getAllSearchedCustomerWallet(String like);

   // public Wallet getWalletByCustomerId(Long customerId);

    public Wallet getCustomerWallet(Long customerId);
}
