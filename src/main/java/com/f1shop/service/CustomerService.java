package com.f1shop.service;

import java.util.List;

import com.f1shop.model.Customer;

public interface CustomerService {
	
	public void add(Customer customer);
	public void edit(Customer customer);
	public void delete(Customer customer);
	public Customer getCustomer(Long customer);
	public List getAllCustomer();

}
