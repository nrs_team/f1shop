/*
@Author : NRS Team
@Date: 2018-04-21
@objective: All the actions related to supplier (insert/update/delete/search) 
 */
package com.f1shop.service.impl;

import com.f1shop.dao.SupplierDao;
import com.f1shop.model.Supplier;
import com.f1shop.service.SupplierService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao; 

    @Transactional
    public void add(Supplier supplier) {
        supplierDao.add(supplier);
    }

    @Transactional
    public void edit(Supplier supplier) {
        supplierDao.edit(supplier);
    }

    @Transactional
    public void delete(Supplier supplier) {
        supplierDao.delete(supplier);
    }

    @Transactional
    public Supplier getSupplier(Long supplierId) {
        return supplierDao.getSupplier(supplierId);
    }

    @Transactional
    public List<Supplier> getAllSupplier() {
        List<Supplier> obj = new ArrayList<>(); 
        obj= supplierDao.getAllSupplier();
//        System.out.println(obj.get(0).getAddress().getAddressOne());
        return obj; 
         
    }

    @Transactional
    public List getAllSearchedSupplier(String like) {
        return supplierDao.getAllSearchedSupplier(like);
    }

    

}
