/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.service.impl;

import com.f1shop.dao.NoticeBoardDao;
import com.f1shop.model.NoticeBoard;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NoticeBoardServiceImpl {
    @Autowired
    private NoticeBoardDao noticeBoardDao;
    
    @Transactional
    public void add(NoticeBoard noticeBoard){
        noticeBoardDao.add(noticeBoard);
    }
    
    @Transactional
    public List getAllNoticeBoard(){
        return noticeBoardDao.getAllNoticeBoard();
//        return null; 
    } 
    
    @Transactional
    public void delete(NoticeBoard noticeBoard){
        noticeBoardDao.delete(noticeBoard);
    }
    
    @Transactional
    public NoticeBoard getNoticeBoard(Long noticeBoardId){
        return noticeBoardDao.getNoticeBoard(noticeBoardId);
    }
    
    @Transactional
    public void update(NoticeBoard noticeBoard){
        noticeBoardDao.update(noticeBoard);
    }
    
    @Transactional
    public List getSearchedNoticeBoards(String like){
         return noticeBoardDao.getSearchedNoticeBoards(like);
    }
}
