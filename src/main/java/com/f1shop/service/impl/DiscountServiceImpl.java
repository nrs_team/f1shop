/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.f1shop.dao.DiscountDao;
import com.f1shop.model.Discount;
import com.f1shop.service.DiscountService;

/**
 *
 * @author Sunil Tako
 */
@Service
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private DiscountDao discountDao;

    @Transactional
    public void add(Discount discount) {
        discountDao.add(discount);
    }

    @Transactional
    public void edit(Discount discount) {
        discountDao.edit(discount);
    }

    @Transactional
    public void delete(Discount discount) {
        discountDao.delete(discount);
    }

    @Transactional
    public Discount getDiscount(Long discountId) {
        return discountDao.getDiscount(discountId);
    }

    @Transactional
    public List getAllDiscount() {
        return discountDao.getAllDiscount();
    }

    @Transactional
    public List getSearchedDiscounts(String like) {
      
        return discountDao.getSearchedDiscounts(like);
        
    }

}
