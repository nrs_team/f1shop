package com.f1shop.service.impl;

import com.f1shop.dao.ShoppingDao;
import com.f1shop.model.OrderDetail;
import com.f1shop.model.OrderItem;
import com.f1shop.model.Product;
import com.f1shop.model.Wallet;
import com.f1shop.service.ShoppingService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Admin
 */
@Service
public class ShoppingServiceImpl implements ShoppingService {

    @Autowired
    private ShoppingDao shoppingDao;

    @Transactional
    public void updateInventory(List<Product> product) {
        shoppingDao.updateInventory(product);
    }

    @Transactional
    public void ProcessPayment(Wallet wallet) {
        shoppingDao.ProcessPayment(wallet);
    }

    @Transactional
    public void addOrderItem(List<OrderItem> orderItem) {
        orderItem.forEach((item) -> {
            shoppingDao.addOrderItem(item);
            
        });
    }

    @Transactional
    public Long addOrderDetail(OrderDetail orderDetail) {
        return shoppingDao.addOrderDetail(orderDetail);
    }

    @Transactional
    public List getAllOrderDetail() {
        return shoppingDao.getAllOrderDetail();
    }

}
