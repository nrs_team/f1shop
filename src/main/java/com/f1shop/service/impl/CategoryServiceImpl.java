package com.f1shop.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.f1shop.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import com.f1shop.dao.CategoryDao;
import com.f1shop.service.CategoryService;
import java.util.List;

/**
 *
 * @author NRS Team
 */
@Service
public class CategoryServiceImpl implements CategoryService{
    
    @Autowired
    private CategoryDao categoryDao;
    
    @Transactional
    public void add(Category category){
        categoryDao.add(category);
    }
    
    @Transactional
    public List getAllCategory(){
        return categoryDao.getAllCategory();
//        return null; 
    } 
    
    @Transactional
    public void delete(Category category){
        categoryDao.delete(category);
    }
    
    @Transactional
    public Category getCategory(Long categoryId){
        return categoryDao.getCategory(categoryId);
    }
    
    @Transactional
    public void update(Category category){
        categoryDao.update(category);
    }
    
    @Transactional
    public List getSearchedCategories(String like){
         return categoryDao.getSearchedCategories(like);
    }
}
