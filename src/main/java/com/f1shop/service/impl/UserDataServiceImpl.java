/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.service.impl;

import com.f1shop.model.UserData;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.f1shop.dao.UserDataDao;
import com.f1shop.service.UserDataService;

/**
 *
 * @author Sunil
 */
@Service
public class UserDataServiceImpl implements UserDataService {
    
    @Autowired
    private UserDataDao userDao;

    @Transactional
    public void add(UserData user) {
        userDao.add(user);
    }

    @Transactional
    public void edit(UserData user) {
        userDao.edit(user);
    }

    @Transactional
    public void delete(UserData user) {
        userDao.delete(user);
    }

    @Transactional
    public UserData getUser(Long user) {
        return userDao.getUser(user);
    }

    @Transactional
    public List getAllUsers() {
        return userDao.getAllUsers();
    }
    
    @Transactional
    public UserData getUserByUserName(String username) {
        return userDao.getUserByUserName(username);
    }
    
}
