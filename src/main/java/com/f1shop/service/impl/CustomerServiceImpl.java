package com.f1shop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.f1shop.dao.CustomerDao;
import com.f1shop.model.Customer;
import com.f1shop.service.CustomerService;
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Transactional
    public void add(Customer customer) {
        customerDao.add(customer);
    }

    @Transactional
    public void edit(Customer customer) {
        customerDao.edit(customer);
    }

    @Transactional
    public void delete(Customer customer) {
        customerDao.delete(customer);
    }

    @Transactional
    public Customer getCustomer(Long customerId) {
        return customerDao.getCustomer(customerId);
    }

    @Transactional
    public List getAllCustomer() {
        return customerDao.getAllCustomer();
    }

}
