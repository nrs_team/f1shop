
package com.f1shop.service.impl;

import com.f1shop.dao.WalletTransactionDao;
import com.f1shop.model.WalletTransaction;
import com.f1shop.service.WalletTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WalletTransactionServiceImpl implements WalletTransactionService {
    @Autowired
    private WalletTransactionDao walletTransactionDao;
    
    @Transactional
    public void add(WalletTransaction walletTransaction){
        walletTransactionDao.add(walletTransaction);
    }
}
