package com.f1shop.service.impl;

import com.f1shop.dao.WalletDao;
import com.f1shop.model.Wallet;
import com.f1shop.service.WalletService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletDao walletDao;

    @Transactional
    public void add(Wallet wallet) {
        walletDao.add(wallet);
    }

    @Transactional
    public void edit(Wallet wallet) {
        walletDao.edit(wallet);
    }

    @Transactional
    public void delete(Wallet wallet) {
        walletDao.delete(wallet);
    }

    @Transactional
    public Wallet getWallet(Long walletId) {
        return walletDao.getWallet(walletId);
    }

    @Transactional
    public List<Wallet> getAllWallet() {
        return walletDao.getAllWallet();
    }

    @Transactional
    public List getAllSearchedCustomerWallet(String like) {
        return walletDao.getAllSearchedCustomerWallet(like);
    }
    
    @Transactional
    public Wallet getCustomerWallet(Long customerId) {
        return walletDao.getCustomerWallet(customerId);
    }

    @Transactional
    public Wallet getWalletByCustomerId(Long customerId) {
        return walletDao.getCustomerWallet(customerId); 
    }

}
