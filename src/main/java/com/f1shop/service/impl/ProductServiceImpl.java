/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.service.impl;

import com.f1shop.dao.ProductDao;
import com.f1shop.model.Product;
import com.f1shop.service.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Transactional
    public List getAllProduct() {
        return productDao.getAllProduct();
    }

    @Transactional
    public void add(Product product) {
        productDao.add(product);
    }

    @Transactional
    public Product getProduct(Long productId) {
        return productDao.getProduct(productId);
    }

    @Transactional
    public void delete(Product product) {
        productDao.delete(product);
    }

    @Transactional
    public List getSearchedProducts(String like) {
        return productDao.getSearchedProducts(like);
    }
    
    @Transactional
    public List getListProductsByCategory(Long categoryId){ 
        return productDao.getListProductsByCategory(categoryId);
    }

    @Transactional
    public List<Product> getSearchedProductsByCategory(String searchedProductName, Long categoryId) {
        return productDao.getSearchedProductsByCategory(searchedProductName, categoryId);
    }
}
