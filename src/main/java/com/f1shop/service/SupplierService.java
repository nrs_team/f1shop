/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.service;

import com.f1shop.model.Supplier;
import java.util.List;

/**
 * List
 *
 * @author Admin
 */
public interface SupplierService {

    public void add(Supplier supplier);

    public void edit(Supplier supplier);

    public void delete(Supplier supplier);

    public Supplier getSupplier(Long supplierId);

    public List<Supplier> getAllSupplier();

    public List getAllSearchedSupplier(String like);

   
}
