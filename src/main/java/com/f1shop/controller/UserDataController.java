package com.f1shop.controller;

import com.f1shop.model.UserData;
import com.f1shop.service.CategoryService;
import com.f1shop.service.UserDataService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/*
@Author : NRS Team
@Date: 2018-5-18
@objective: All the actions related to users (insert/update/delete/search) 
 */
@Controller
public class UserDataController {

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = {"/admin/allUsers", "/admin/users"}, method = RequestMethod.GET)
    public ModelAndView allUsers(ModelAndView modelAndView) {
        modelAndView.setViewName("administration/users");

        UserData userData = new UserData();
        modelAndView.addObject("userData", userData);
        List<UserData> userList = userDataService.getAllUsers();
        modelAndView.addObject("userList", userList);
        modelAndView.addObject("categoryList", categoryService.getAllCategory());
        return modelAndView;

    }

    @RequestMapping("/admin/user/create")
    public String create(Model model) {
        UserData userData = new UserData();
        model.addAttribute(userData);
        model.addAttribute("categoryList", categoryService.getAllCategory());
        return "administration/createUser";
    }

}
