/*
     F1-SC-C-3: The Category controller is responsible for manipulating the information regarding Category. It observe the
                 interaction of adminin placing the category and act upon it.
 */
package com.f1shop.controller;

import com.f1shop.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.f1shop.service.CategoryService;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import javafx.beans.binding.Binding;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CategoryController {

    private Path path;
    @Autowired
    private CategoryService categoryService;
    private Date date = new Date();

    @RequestMapping("/admin/categories")
    public ModelAndView setupFrom(ModelAndView modelAndView) {
        //for the path to the image
        Category category = new Category();
        modelAndView.addObject("category", category);
        modelAndView.addObject("categoryList", categoryService.getAllCategory());
        
        modelAndView.setViewName("category/list");
        return modelAndView;
    }

    @RequestMapping("/admin/addCategory")
    public ModelAndView createNewCategory(ModelAndView modelAndView) {
        Category category = new Category();
        modelAndView.setViewName("category/create");
        modelAndView.addObject("category", category);
        modelAndView.addObject("categoryList", categoryService.getAllCategory());
        return modelAndView;

    }

    //if there requires for the method with post define then call for the RequestMethod class.
    @RequestMapping(value = {"/admin/category/create"}, method = RequestMethod.POST)
    public String doActions(@Valid @ModelAttribute("category") Category category, @RequestParam("categoryImage") MultipartFile categoryImage, BindingResult bindingResult, HttpServletRequest request) {
        //check if there is problem in binding
        if (bindingResult.hasErrors()) {
            return "redirect:/admin/categories";
        }
        try {
            category.setCreatedDate(date);
            categoryService.add(category);
            //****************************for the image section************************
            //MultipartFile categoryImage = category.getCategoryImage();
        
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "\\assests\\image\\category\\"+category.getId()+".png");

            // ---------------------------------testing --------------------------------------
            File file = new File(rootDirectory + "\\assests\\image\\category\\"+category.getId()+".png");
            //check the folder and if not present create
            if(!Files.exists(path)){
            	file.getParentFile().mkdirs();
            }
            //-----------------------------------end of testing-------------------------------

             if (categoryImage !=null && !categoryImage.isEmpty()) {
                 try {
                     FileCopyUtils.copy(categoryImage.getBytes(), new File(path.toString()));
 //                    categoryImage.transferTo(new File(path.toString()));
                 } catch (Exception e) {
                     e.printStackTrace();
                     throw new RuntimeException("Category image saving failed", e);
                 }
             }
            
            //****************************end of the image section***********************
            return "redirect:/admin/categories";
            } catch (Exception ex) {
                ex.printStackTrace();
                return "redirect:/admin/categories";
            }

    }

    //for the deletion of the category value
    @RequestMapping(value = {"/admin/deleteCategory/{id}"}, method = RequestMethod.GET)
    public String deleteCategory(ModelAndView modelAndView, @PathVariable("id") int id) {
        Category category = categoryService.getCategory(Long.valueOf(id));
        category.setIsActive(false);
        category.setModifiedDate(date);
        categoryService.delete(category);
        //delete the image
        File imageFile = new File("");
        imageFile.delete();
        return "redirect:/admin/categories";
    }

    //for the page for the user to edit the category
    @RequestMapping(value = {"/admin/editCategory/{id}"}, method = RequestMethod.GET)
    public ModelAndView editCategory(ModelAndView modelAndView, @PathVariable("id") int id) {
        Category category = categoryService.getCategory(Long.valueOf(id));
        modelAndView.setViewName("category/edit");
        modelAndView.addObject("category", category);

        return modelAndView;
    }

    //for the updating the category information
    @RequestMapping(value = {"/admin/category/update/{id}"}, method = RequestMethod.POST)
    public String update(@PathVariable("id") final long id, @ModelAttribute("category") Category category, @RequestParam("categoryImage") MultipartFile categoryImage, HttpServletRequest request) {
        try {
            category.setId(id);
            category.setModifiedDate(date);
            categoryService.update(category);
            //****************************for the image section************************
            //MultipartFile categoryImage = category.getCategoryImage();
        
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "\\assests\\image\\category\\"+category.getId()+".png");

            // ---------------------------------testing --------------------------------------
            File file = new File(rootDirectory + "\\assests\\image\\category\\"+category.getId()+".png");
            //check the folder and if not present create
           file.delete();
            //-----------------------------------end of testing-------------------------------

             if (categoryImage !=null && !categoryImage.isEmpty()) {
                 try {
                     FileCopyUtils.copy(categoryImage.getBytes(), new File(path.toString()));
 //                    categoryImage.transferTo(new File(path.toString()));
                 } catch (Exception e) {
                     e.printStackTrace();
                     throw new RuntimeException("Category image saving failed", e);
                 }
             }
            
            //****************************end of the image section***********************
            return "redirect:/admin/categories";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/categories";
        }
    }

    //for displaying the category specific information
    @RequestMapping(value = {"/admin/viewCategory/{id}"}, method = RequestMethod.GET)
    public ModelAndView view(ModelAndView modelAndView, @PathVariable("id") int id) {
        Category category = categoryService.getCategory(Long.valueOf(id));
        modelAndView.setViewName("category/view");
        modelAndView.addObject("category", category);
        //to display active inactive instead of 0 and 1
        if (category.getIsActive()) {
            modelAndView.addObject("isActive", "checked");
        } else {
            modelAndView.addObject("isActive", "");
        }
        return modelAndView;
    }

    //for the searching the specific categories
    @RequestMapping(value = {"/admin/category/search"}, method = RequestMethod.POST)
    public ModelAndView search(@RequestParam("name") String searchedCategoryName, ModelAndView modelAndView) {
        modelAndView.setViewName("category/list");
        modelAndView.addObject("categoryList", categoryService.getSearchedCategories(searchedCategoryName));
        return modelAndView;
    }

}
