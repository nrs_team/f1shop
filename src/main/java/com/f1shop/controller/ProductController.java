package com.f1shop.controller;
/*
    F1-SC-C-8: The product controller works for the CRUD operation of product. Here, there is one to one relation with supplier 
                and one to many relations with category.
*/

import com.f1shop.model.Product;
import com.f1shop.service.CategoryService;
import com.f1shop.service.ProductService;
import com.f1shop.service.SupplierService;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {
    private Path path;
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SupplierService supplierService;
    Date date = new Date();

    @RequestMapping("admin/products")
    public ModelAndView displayProducts(ModelAndView modelAndView) {
        Product product = new Product();
        // modelAndView.addObject("product", product);
        modelAndView.addObject("productList", productService.getAllProduct());
        modelAndView.addObject("categoryList", categoryService.getAllCategory());
        modelAndView.setViewName("product/list");
        return modelAndView;
    }

    @RequestMapping("admin/product/add")
    public ModelAndView createProductPage(ModelAndView modelAndView) {
        Product product = new Product();
        List category = categoryService.getAllCategory();
        List supplier = supplierService.getAllSupplier();
        modelAndView.setViewName("product/create");
        modelAndView.addObject("categoryList", category);
        modelAndView.addObject("supplierList", supplier);
        modelAndView.addObject("product", product);
        return modelAndView;
    }

    @RequestMapping(value = {"admin/product/create"}, method = RequestMethod.POST)
    public String add(@ModelAttribute("product") Product product, @RequestParam("productImage") MultipartFile productImage, BindingResult bindingResult, HttpServletRequest request) {
        try {
            product.setIsActive(true);
            product.setCreatedDate(date);
            productService.add(product);
            //****************************for the image section************************
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "\\assests\\image\\product\\"+product.getId()+".png");

            // ---------------------------------testing --------------------------------------
            File file = new File(rootDirectory + "\\assests\\image\\product\\"+product.getId()+".png");
            //check the folder and if not present create
            if(!Files.exists(path)){
            	file.getParentFile().mkdirs();
            }
            //-----------------------------------end of testing-------------------------------

             if (productImage !=null && !productImage.isEmpty()) {
                 try {
                     FileCopyUtils.copy(productImage.getBytes(), new File(path.toString()));
 //                    categoryImage.transferTo(new File(path.toString()));
                 } catch (Exception e) {
                     e.printStackTrace();
                     throw new RuntimeException("Category image saving failed", e);
                 }
             }
            
            //****************************end of the image section***********************
            return "redirect:/admin/products";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/products";
        }

    }

    @RequestMapping(value = {"admin/product/delete/{id}"}, method = RequestMethod.GET)
    public String deleteProduct(ModelAndView modelAndView, @PathVariable("id") final long id) {
        Product product = productService.getProduct(id);
        product.setIsActive(false);
        product.setCreatedDate(date);
        productService.delete(product);
        return "redirect:/admin/products";
    }

    @RequestMapping(value = {"admin/product/view/{id}"}, method = RequestMethod.GET)
    public String viewProduct(@PathVariable("id") final long id, Model model) {
        Product obj = productService.getProduct(id);
        model.addAttribute(obj);
        return "product/view";
    }

    @RequestMapping(value = {"/admin/product/edit/{id}"}, method = RequestMethod.GET)
    public ModelAndView editProduct(ModelAndView modelAndView, @PathVariable("id") int id) {
        Product product = productService.getProduct(Long.valueOf(id));
        List category = categoryService.getAllCategory();
        List supplier = supplierService.getAllSupplier();
        modelAndView.setViewName("product/edit");
        modelAndView.addObject("categoryList", category);
        modelAndView.addObject("supplierList", supplier);
        modelAndView.addObject("product", product);
        return modelAndView;
    }

    @RequestMapping(value = {"admin/product/update/{id}"}, method = RequestMethod.POST)
    public String updateProduct(@ModelAttribute("product") Product product, @PathVariable("id") final long id, @RequestParam("productImage") MultipartFile productImage, HttpServletRequest request) {
        product.setId(id);
        product.setModifiedDate(date);
        //both update and delete does the same things
        productService.delete(product);
        //****************************for the image section************************        
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "\\assests\\image\\product\\"+product.getId()+".png");

            // ---------------------------------testing --------------------------------------
            File file = new File(rootDirectory + "\\assests\\image\\product\\"+product.getId()+".png");
            //check the folder and if not present create
           file.delete();
            //-----------------------------------end of testing-------------------------------

             if (productImage !=null && !productImage.isEmpty()) {
                 try {
                     FileCopyUtils.copy(productImage.getBytes(), new File(path.toString()));
 //                    categoryImage.transferTo(new File(path.toString()));
                 } catch (Exception e) {
                     e.printStackTrace();
                     throw new RuntimeException("Category image saving failed", e);
                 }
             }
            
            //****************************end of the image section***********************
        return "redirect:/admin/products";
    }
    
    @RequestMapping(value = {"admin/product/search"}, method = RequestMethod.POST)
    public ModelAndView search(@RequestParam("searchedProductName") String searchedProductName, ModelAndView modelAndView){
        modelAndView.setViewName("product/list");
        modelAndView.addObject("productList", productService.getSearchedProducts(searchedProductName));        
        return modelAndView;
    }
}
