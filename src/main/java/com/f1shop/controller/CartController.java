package com.f1shop.controller;

/*
    F1-SC-C-1: This controller takes the task of manipulating the cart information that end-user order the product from their
                panel.
                
 */

import com.f1shop.dao.CartDao;
import com.f1shop.model.Product;
import com.f1shop.model.UserData;
import com.f1shop.model.Wallet;
import com.f1shop.model.pojo.Cart;
import com.f1shop.model.pojo.CartItem;
import com.f1shop.service.CustomerService;
import com.f1shop.service.ProductService;
import com.f1shop.service.UserDataService;
import com.f1shop.service.WalletService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

//annotation to call controller
@Controller
//display to call this controller
@RequestMapping("/rest/cart")
public class CartController {

    //list of attributes
    @Autowired
    private CartDao cartDao;

    @Autowired
    private ProductService productService;

    @Autowired
    private WalletService walletService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserDataService userDataService;

    private String walletId = "3";

    //show the number of item in the cart
    @RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
    public @ResponseBody
    Cart read(@PathVariable(value = "cartId") String cartId) {

        return cartDao.read(cartId);
    }

    //updates the number of cart items
    @RequestMapping(value = "/{cartId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void update(@PathVariable(value = "cartId") String cartId, @RequestBody Cart cart) {

        cartDao.update(cartId, cart);
    }

    //the cart items get deleted
    @RequestMapping(value = "/{cartId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(value = "cartId") String cartId) {
        cartDao.delete(cartId);
    }

    //add the product in the cart 
    @RequestMapping(value = "/add/{productId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void addItem(@PathVariable(value = "productId") Long productId, HttpServletRequest request) {
        String sessionId = request.getSession(true).getId();

        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        UserData userData = (UserData) userDataService.getUserByUserName(username);
        Wallet walletObj = new Wallet(); 
        walletObj = walletService.getCustomerWallet(userData.getCustomer().getId());
       // double balance = Double.parseDouble("" + walletObj.getAvailableBalance());
        Cart cart = cartDao.read(sessionId);
        if (cart == null) {
            cart = cartDao.create(new Cart(sessionId, userData, walletObj));
        }
        Product product = productService.getProduct(productId);

        if (product == null) {
            throw new IllegalArgumentException(String.format("Unable to find, Product {0} doesn't exists", productId));
        }

        cart.addCartItem(new CartItem(product));
        cartDao.update(sessionId, cart);
    }

    @RequestMapping(value = "/remove/{productId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeIteam(@PathVariable(value = "productId") Long productId, HttpServletRequest request) {
        String sessionId = request.getSession(true).getId();
        Cart cart = cartDao.read(sessionId);
        if (cart == null) {
            cart = cartDao.create(new Cart(sessionId));
        }
        Product product = productService.getProduct(productId);
        if (product == null) {
            throw new IllegalArgumentException(String.format("Unable to find, Product {0} doesn't exists", productId));
        }
        cart.removeCartItem(new CartItem(product));
        cartDao.update(sessionId, cart);
    }

    //reduce the number of product from the cart
    @RequestMapping(value = "/reduce/{productId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void reduceIteam(@PathVariable(value = "productId") Long productId, HttpServletRequest request) {
        String sessionId = request.getSession(true).getId();
        Cart cart = cartDao.read(sessionId);
        if (cart == null) {
            cart = cartDao.create(new Cart(sessionId));
        }
        Product product = productService.getProduct(productId);
        if (product == null) {
            throw new IllegalArgumentException(String.format("Unable to find, Product {0} doesn't exists", productId));
        }
        cart.reduceCartItem(new CartItem(product));
        cartDao.update(sessionId, cart);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Illigal Request, please verify data again")
    public void handleClientError(Exception e) {

    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal server error")
    public void handleServerError(Exception e) {

    }
}
