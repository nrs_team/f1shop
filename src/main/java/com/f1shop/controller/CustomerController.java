package com.f1shop.controller;
/*
        F1-SC-C-4: The customer controller list down and act upon Create Read Update and Delete activities.

*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.f1shop.model.Customer;
import com.f1shop.service.CustomerService;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerservice;

    @RequestMapping(value = {"/admin/customers"}, method = RequestMethod.GET)
    public ModelAndView setupFrom(ModelAndView modelAndView) {

        modelAndView.setViewName("customer");

        Customer customer = new Customer();

        modelAndView.addObject("customer", customer);
        modelAndView.addObject("customerList", customerservice.getAllCustomer());

        return modelAndView;
    }

    @RequestMapping(value = "/admin/customer1.do", method = RequestMethod.POST)
    public ModelAndView doActions(@ModelAttribute Customer customer, BindingResult result, @RequestParam String action, ModelAndView modelAndView) {

        modelAndView.setViewName("customer");

        Customer customerResult = new Customer();

        switch (action.toLowerCase()) {

            case "add":
                customerservice.add(customer);

                customerResult = customer;
                break;
            case "edit":
                customerservice.edit(customer);

                customerResult = customer;
                break;
            case "delete":
                customerservice.delete(customer);

                customerResult = new Customer();
                break;
            case "search":
                Customer searchedCustomer = customerservice.getCustomer(customer.getId());

                customerResult = searchedCustomer != null ? searchedCustomer : new Customer();
                break;
        }

        modelAndView.addObject("customer", customerResult);
        modelAndView.addObject("customerList", customerservice.getAllCustomer());

        return modelAndView;
    }

}
