
package com.f1shop.controller;

import com.f1shop.model.UserData;
import com.f1shop.model.Wallet;
import com.f1shop.model.WalletTransaction;
import com.f1shop.service.UserDataService;
import com.f1shop.service.WalletService;
import com.f1shop.service.WalletTransactionService;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WalletTransactionController {
    @Autowired
    private WalletTransactionService walletTransactionService;
    @Autowired
    private WalletService walletService;
    @Autowired
    private UserDataService userDataService;
    
    private Date date = new Date();
    
    @RequestMapping(value = {"/wallet/toppedup"}, method = RequestMethod.POST)
    public String customertoppedup(@ModelAttribute("walletTransaction") WalletTransaction walletTransaction, HttpServletRequest request) {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        UserData userData = (UserData) userDataService.getUserByUserName(username);
        walletTransaction.setCreatedDate(date);
        walletTransaction.setCustomerId(userData.getCustomer().getId());
        walletTransactionService.add(walletTransaction);
        
        //for the wallet amount update
        Wallet wallet = walletService.getCustomerWallet(userData.getCustomer().getId());
        wallet.setAvailableBalance((Double.parseDouble(walletTransaction.getAmount()) + wallet.getAvailableBalance()));
        walletService.edit(wallet);
        return "redirect:/shopping/cart/" + request.getSession(true).getId();
    }
}
