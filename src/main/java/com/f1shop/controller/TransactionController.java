/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.controller;

import com.f1shop.model.Discount;
import com.f1shop.model.OrderDetail;
import com.f1shop.model.OrderItem;
import com.f1shop.service.ShoppingService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lenovo
 */
@Controller
public class TransactionController {
    
    @Autowired
    private ShoppingService shoppingService;

    @RequestMapping(value = {"/admin/transactions", "/admin/allTransaction"}, method = RequestMethod.GET)
    public ModelAndView allDiscounts(ModelAndView modelAndView) {
        modelAndView.setViewName("report/transaction");

        List<OrderItem> orderItemList = shoppingService.getAllOrderDetail();
        modelAndView.addObject("orderItemList", orderItemList);
       
        return modelAndView;

    }
}
