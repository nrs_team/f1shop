package com.f1shop.controller;

/*
    F1-SC-C-2: This controller is prepared to maintain at the time of placing products in the cart. The information of the
                Wallet of the individual customer is required to show. If checkout is done then amount need to be updated.
 */
import com.f1shop.dao.CartDao;
import com.f1shop.model.Customer;
import com.f1shop.model.UserData;
import com.f1shop.model.Wallet;
import com.f1shop.service.CategoryService;
import com.f1shop.service.CustomerService;
import com.f1shop.service.SupplierService;
import com.f1shop.service.UserDataService;
import com.f1shop.service.WalletService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/shopping/cart")
public class CartItemController {

    //attributes for having the wallet information.
    @Autowired
    private WalletService walletService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserDataService userDataService;

    @RequestMapping
    public String get(HttpServletRequest request) {
        return "redirect:/shopping/cart/" + request.getSession(true).getId();
    }

    @RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
    public String getCart(@PathVariable(value = "cartId") String cartId, Model model) {
        model.addAttribute("cartId", cartId);
        return "shopping/cart";
    }

    @RequestMapping(value = "checkout/{cartId}", method = RequestMethod.GET)
    public String checkout(@PathVariable(value = "cartId") String cartId, Model model) {
//        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
//
//        UserData userData = (UserData) userDataService.getUserByUserName(username);
//        if(userData.getCustomer().getAddress()==null){
//            String message = "Delivery address is not available, please update your address"; 
//            model.addAttribute("message",  message); 
//        }
//        Wallet wallet = walletService.getWallet(Long.parseLong("1"));
//        model.addAttribute("cartId", cartId);
//        model.addAttribute("wallet", wallet);
//        model.addAttribute("userData", userData); 
        
        return "shopping/checkout";
    }

}
