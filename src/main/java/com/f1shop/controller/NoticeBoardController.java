/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1shop.controller;

import com.f1shop.model.NoticeBoard;
import com.f1shop.service.NoticeBoardService;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class NoticeBoardController {
    private Path path;
    @Autowired
    private NoticeBoardService noticeBoardService;
    private Date date = new Date();
    
    @RequestMapping("/admin/noticeboards")
    public ModelAndView setupFrom(ModelAndView modelAndView) {
        //for the path to the image
        NoticeBoard noticeBoard = new NoticeBoard();
        modelAndView.addObject("noticeBoard", noticeBoard);
        modelAndView.addObject("noticeBoardList", noticeBoardService.getAllNoticeBoard());
        
        modelAndView.setViewName("noticeBoard/list");
        return modelAndView;
    }
    
    @RequestMapping("/admin/addNoticeBoard")
    public ModelAndView createNewNoticeBoard(ModelAndView modelAndView) {
        NoticeBoard noticeBoard = new NoticeBoard();
        modelAndView.setViewName("noticeBoard/create");
        modelAndView.addObject("noticeBoard", noticeBoard);
        modelAndView.addObject("noticeBoardList", noticeBoardService.getAllNoticeBoard());
        return modelAndView;

    }
    
    //if there requires for the method with post define then call for the RequestMethod class.
    @RequestMapping(value = {"/admin/noticeBoard/create"}, method = RequestMethod.POST)
    public String doActions(@Valid @ModelAttribute("noticeBoard") NoticeBoard noticeBoard, @RequestParam("noticeBoardImage") MultipartFile noticeBoardImage, BindingResult bindingResult, HttpServletRequest request) {
        //check if there is problem in binding
        if (bindingResult.hasErrors()) {
            return "redirect:/admin/noticeBoards";
        }
        try {
            noticeBoard.setCreatedDate(date);
            noticeBoardService.add(noticeBoard);
            //****************************for the image section************************
            //MultipartFile noticeBoardImage = noticeBoard.getNoticeBoardImage();
        
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "\\assests\\image\\noticeBoard\\"+noticeBoard.getId()+".png");

            // ---------------------------------testing --------------------------------------
            File file = new File(rootDirectory + "\\assests\\image\\noticeBoard\\"+noticeBoard.getId()+".png");
            //check the folder and if not present create
            if(!Files.exists(path)){
            	file.getParentFile().mkdirs();
            }
            //-----------------------------------end of testing-------------------------------

             if (noticeBoardImage !=null && !noticeBoardImage.isEmpty()) {
                 try {
                     FileCopyUtils.copy(noticeBoardImage.getBytes(), new File(path.toString()));
 //                    noticeBoardImage.transferTo(new File(path.toString()));
                 } catch (Exception e) {
                     e.printStackTrace();
                     throw new RuntimeException("Notice Board image saving failed", e);
                 }
             }
            
            //****************************end of the image section***********************
            return "redirect:/admin/noticeBoards";
            } catch (Exception ex) {
                ex.printStackTrace();
                return "redirect:/admin/noticeBoards";
            }

    }
    
    //for the deletion of the noticeBoard value
    @RequestMapping(value = {"/admin/deleteNoticeBoard/{id}"}, method = RequestMethod.GET)
    public String deleteNoticeBoard(ModelAndView modelAndView, @PathVariable("id") int id) {
        NoticeBoard noticeBoard = noticeBoardService.getNoticeBoard(Long.valueOf(id));
        noticeBoard.setStatus(false);
        noticeBoard.setModifiedDate(date);
        noticeBoardService.delete(noticeBoard);
        //delete the image
        File imageFile = new File("");
        imageFile.delete();
        return "redirect:/admin/noticeBoards";
    }

    //for the page for the user to edit the noticeBoard
    @RequestMapping(value = {"/admin/editNoticeBoard/{id}"}, method = RequestMethod.GET)
    public ModelAndView editNoticeBoard(ModelAndView modelAndView, @PathVariable("id") int id) {
        NoticeBoard noticeBoard = noticeBoardService.getNoticeBoard(Long.valueOf(id));
        modelAndView.setViewName("noticeBoard/edit");
        modelAndView.addObject("noticeBoard", noticeBoard);

        return modelAndView;
    }
    
    //for the updating the noticeBoard information
    @RequestMapping(value = {"/admin/noticeBoard/update/{id}"}, method = RequestMethod.POST)
    public String update(@PathVariable("id") final long id, @ModelAttribute("noticeBoard") NoticeBoard noticeBoard, @RequestParam("noticeBoardImage") MultipartFile noticeBoardImage, HttpServletRequest request) {
        try {
            noticeBoard.setId(id);
            noticeBoard.setModifiedDate(date);
            noticeBoardService.update(noticeBoard);
            //****************************for the image section************************
            //MultipartFile noticeBoardImage = noticeBoard.getNoticeBoardImage();
        
            String rootDirectory = request.getSession().getServletContext().getRealPath("/");
            path = Paths.get(rootDirectory + "\\assests\\image\\noticeBoard\\"+noticeBoard.getId()+".png");

            // ---------------------------------testing --------------------------------------
            File file = new File(rootDirectory + "\\assests\\image\\noticeBoard\\"+noticeBoard.getId()+".png");
            //check the folder and if not present create
           file.delete();
            //-----------------------------------end of testing-------------------------------

             if (noticeBoardImage !=null && !noticeBoardImage.isEmpty()) {
                 try {
                     FileCopyUtils.copy(noticeBoardImage.getBytes(), new File(path.toString()));
 //                    noticeBoardImage.transferTo(new File(path.toString()));
                 } catch (Exception e) {
                     e.printStackTrace();
                     throw new RuntimeException("NoticeBoard image saving failed", e);
                 }
             }
            
            //****************************end of the image section***********************
            return "redirect:/admin/categories";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/categories";
        }
    }

    //for displaying the noticeBoard specific information
    @RequestMapping(value = {"/admin/viewNoticeBoard/{id}"}, method = RequestMethod.GET)
    public ModelAndView view(ModelAndView modelAndView, @PathVariable("id") int id) {
        NoticeBoard noticeBoard = noticeBoardService.getNoticeBoard(Long.valueOf(id));
        modelAndView.setViewName("noticeBoard/view");
        modelAndView.addObject("noticeBoard", noticeBoard);
        //to display active inactive instead of 0 and 1
        if (noticeBoard.getStatus()) {
            modelAndView.addObject("status", "checked");
        } else {
            modelAndView.addObject("status", "");
        }
        return modelAndView;
    }

    //for the searching the specific categories
    @RequestMapping(value = {"/admin/noticeBoard/search"}, method = RequestMethod.POST)
    public ModelAndView search(@RequestParam("name") String searchedNoticeBoardName, ModelAndView modelAndView) {
        modelAndView.setViewName("noticeBoard/list");
        modelAndView.addObject("noticeBoardList", noticeBoardService.getSearchedNoticeBoards(searchedNoticeBoardName));
        return modelAndView;
    }
}
