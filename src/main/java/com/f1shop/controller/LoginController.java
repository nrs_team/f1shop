package com.f1shop.controller;
/*
        F1-SC-C-6: The Login controller mainly look after the login authentication for the user access control.
*/

import com.f1shop.model.UserData;
import com.f1shop.service.UserDataService;
import com.f1shop.model.util.Login;
import com.f1shop.model.util.UserInfo;
import com.f1shop.service.impl.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class LoginController {

    @Autowired
    private UserDataService userDataService;

    //check the user has provided authenticated login and password.
    @RequestMapping("/login")
    public String loging(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout, Model model) {

        if (error != null) {
            model.addAttribute("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addAttribute("msg", "You have been logged out successfully.");
        }
        
//        UserInfo userinfo = new UserInfo();
//        
//        UserData currentUser = userinfo.getCurrentUser();
//        
//        model.addAttribute("currentUser", currentUser);
//        
//
//        UserData user1 = (UserData) userDataService.getUserByUserName("sunil").get(0);
//
//        System.out.println("User Id" + user1.getId());

        return "login";
    }
    //if the user like to register in the system
    @RequestMapping("/register")
    public String registered(Model model) {

        UserData userData = new UserData();

        model.addAttribute("userData", userData);

        return "/register";

    }

    @RequestMapping(name = "/user/register", method = RequestMethod.POST)
    public String getRegistered(@ModelAttribute("user") UserData userData) {

        Login loggedIn = new Login();

        boolean isSignupped = loggedIn.signup(userData);

        if (isSignupped) {
            userDataService.add(userData);
        }

        return "shopping/products";

    }
    //for the customer to look and edit their profile
    @RequestMapping("/customer/profile/{id}")
    public String customerPage(@PathVariable("id") Long id, Model model) {

        UserData obj = userDataService.getUser(id);

        model.addAttribute("userdata", obj);

        return "/customer/profile";

    }

}
