package com.f1shop.controller;
/*
    F1-SC-C-10: Suppliers Controller has the CRUD operations. Here, there is automatic saving the address information of them too.
*/
import com.f1shop.model.Supplier;
import com.f1shop.service.CategoryService;
import com.f1shop.service.SupplierService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/*
@Author : NRS Team
@Date: 2018-04-21
@objective: All the actions related to supplier (insert/update/delete/search) 
 */
@Controller
public class SuppliersController {

    @Autowired
    private SupplierService supplierService;
    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = {"/admin/allsuppliers", "/admin/suppliers"}, method = RequestMethod.GET)
    public ModelAndView allSuppliers(ModelAndView modelAndView) {
        modelAndView.setViewName("supplier/list");

        Supplier supplier = new Supplier();
        modelAndView.addObject("supplier", supplier);
        List<Supplier> allSuppliers = supplierService.getAllSupplier();
        modelAndView.addObject("categoryList", categoryService.getAllCategory());
        modelAndView.addObject("supplierList", allSuppliers);
        return modelAndView;

    }

    @RequestMapping("/admin/supplier/create")
    public String create(Model model) {
        Supplier supplier = new Supplier();
        model.addAttribute(supplier);
        model.addAttribute("categoryList", categoryService.getAllCategory());
        return "supplier/create";
    }

    @RequestMapping(value = {"admin/supplier/create"}, method = RequestMethod.POST)
    public String add(@ModelAttribute("supplier") Supplier supplier) {

        try {
            supplier.setIsActive(true);
            supplierService.add(supplier);

            return "redirect:/admin/suppliers";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/suppliers";
        }
    }

    @RequestMapping(value = {"admin/supplier/view/{id}"}, method = RequestMethod.GET)
    public String view(@PathVariable("id") final long id, Model model) {
        Supplier obj = supplierService.getSupplier(id);
        model.addAttribute(obj);
        model.addAttribute("categoryList", categoryService.getAllCategory());
        return "supplier/view";
    }

    @RequestMapping(value = {"admin/supplier/edit/{id}"}, method = RequestMethod.GET)
    public String edit(@PathVariable("id") final long id, Model model) {
        Supplier obj = supplierService.getSupplier(id);
        model.addAttribute(obj);
        model.addAttribute("categoryList", categoryService.getAllCategory());
        return "supplier/edit";
    }

    @RequestMapping(value = {"admin/supplier/edit/{id}"}, method = RequestMethod.POST)
    public String update(@PathVariable("id") final long id, @ModelAttribute("supplier") Supplier supplier) {
        try {
            supplier.setId(id);
            supplierService.edit(supplier);
            return "redirect:/admin/suppliers";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/suppliers";
        }

    }

    @RequestMapping(value = {"admin/supplier/delete/{id}"}, method = RequestMethod.GET)
    public String delete(@PathVariable("id") final long id) {
        Supplier obj = supplierService.getSupplier(id);
        obj.setIsActive(false);
        supplierService.edit(obj);
        return "redirect:/admin/suppliers";
    }

}
