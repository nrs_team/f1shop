/*
  F1-SC-C-7: The Page Navigation Controller shows the way either to display around admin, customer or login.
 */
package com.f1shop.controller;

import com.f1shop.model.UserData;
import com.f1shop.model.util.UserInfo;
import com.f1shop.service.CategoryService;
import com.f1shop.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sunil Tako
 */
@Controller
public class PageNavigationController {
    @Autowired
    private CategoryService categoryService;
    
    @Autowired
    private UserDataService userService;
    
    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public ModelAndView indexPage(ModelAndView modelAndView) {

        modelAndView.setViewName("index");
        modelAndView.addObject("cart", 5); 
         modelAndView.addObject("categoryList", categoryService.getAllCategory());
        return modelAndView;

    }

    @RequestMapping(value = "/adminPanel", method = RequestMethod.GET)
    public ModelAndView adminPanel(ModelAndView modelAndView) {

        UserInfo userInfo = new UserInfo();
        UserData userData = userInfo.getCurrentUser();
        
        modelAndView.addObject("userData", userData);
        
        modelAndView.setViewName("adminPanel");

        return modelAndView;

    }

    @RequestMapping(value = "/customerPanel", method = RequestMethod.GET)
    public ModelAndView customerPanel(ModelAndView modelAndView) {

        modelAndView.setViewName("customerPanel");

        return modelAndView;

    }
    
    @RequestMapping(value = "/imageUpload", method = RequestMethod.GET)
    public ModelAndView imageUpload(ModelAndView modelAndView) {

        modelAndView.setViewName("imageUpload");

        return modelAndView;

    }

}
