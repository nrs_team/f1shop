package com.f1shop.controller;
/*
    F1-SC-C-9: Shopping Controller is used for the shopping of the customer. It acts upon the activities where customer like to
                do shopping from the system.
*/
import com.f1shop.dao.CartDao;
import com.f1shop.model.Discount;
import com.f1shop.model.OrderDetail;
import com.f1shop.model.OrderItem;
import com.f1shop.model.Product;
import com.f1shop.model.UserData;
import com.f1shop.model.pojo.Cart;
import com.f1shop.model.pojo.CartItem;
import com.f1shop.service.CategoryService;
import com.f1shop.service.DiscountService;
import com.f1shop.service.ProductService;
import com.f1shop.service.ShoppingService;
import com.f1shop.service.UserDataService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ShoppingController {

    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private DiscountService discountService;

    @Autowired
    private CartDao cartDao;
    @Autowired
    private UserDataService userDataService;

    @Autowired
    private ShoppingService shoppingService;

    @RequestMapping(value = {"/shopping","/shopping/"}, method = RequestMethod.GET)
    public ModelAndView shoppingitems(ModelAndView modelAndView) {

        modelAndView.setViewName("shopping/products");
        Product supplier = new Product();
        modelAndView.addObject("supplier", supplier);
        List<Product> allProducts = productService.getAllProduct();
        modelAndView.addObject("productList", allProducts);
        modelAndView.addObject("total", allProducts.size());
        modelAndView.addObject("categoryList", categoryService.getAllCategory());

        //${filter} to show the text filtered
        //modelAndView.addObject("cart", 5); add this to show the cart item in icon
        //modelAndView.addObject("success", "XXXX") ; //add this to show the message dialog, other inputs can be fail, and alert
        return modelAndView;

    }
    
    //category wise product display
    @RequestMapping(value = {"/shopping/{id}"}, method = RequestMethod.GET)
    public ModelAndView shoppingitems(ModelAndView modelAndView, @PathVariable("id") Long categoryId) {

        modelAndView.setViewName("shopping/products");
        Product supplier = new Product();
        modelAndView.addObject("supplier", supplier);
        List<Product> allProducts = productService.getListProductsByCategory(categoryId);
        modelAndView.addObject("productList", allProducts);
        modelAndView.addObject("total", allProducts.size());
        modelAndView.addObject("categoryId", categoryId);
        modelAndView.addObject("categoryList", categoryService.getAllCategory());

        //${filter} to show the text filtered
        //modelAndView.addObject("cart", 5); add this to show the cart item in icon
        //modelAndView.addObject("success", "XXXX") ; //add this to show the message dialog, other inputs can be fail, and alert
        return modelAndView;

    }
    
    

    //payment and checkout method
    @RequestMapping(value = {"/shopping/checkout/{cartId}"}, method = RequestMethod.GET)
    public ModelAndView checkout(ModelAndView modelAndView, @PathVariable(value = "cartId") String cartId) {
        
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()). getUsername();

        UserData userData = (UserData) userDataService.getUserByUserName(username);

        if (doesUserHasAddress(userData)) {
            
            
            modelAndView.addObject("hasAddress", doesUserHasAddress(userData));

            modelAndView.setViewName("shopping/receipt");

            return modelAndView;

        } else {

            Cart cart = cartDao.read(cartId);

            OrderItem orderItem = new OrderItem();
            List<OrderItem> orderItemList = new ArrayList<>();
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setNumberOfOrder(cart.getTotalItemCount());
            orderDetail.setCreatedDate(new Date());
            orderDetail.setStatus(0);
            orderDetail.setRemarks("New order");
            orderDetail.setTotalPrice(cart.getGrandTotal());

            orderDetail.setBillingAddressId(userData.getCustomer().getAddress().getId());
            orderDetail.setShippingAddressId(userData.getCustomer().getAddress().getId());
            orderDetail.setCustomerId(userData.getCustomer().getId());
            Long orderDetailId;
            orderDetailId = shoppingService.addOrderDetail(orderDetail);
            List<CartItem> cartItemList;
            cartItemList = new ArrayList<>(cart.getCartItems().values());

            Long productId = Long.parseLong("2");
            Long discountId = Long.parseLong("1");
            for (CartItem CartItemObj : cartItemList) {
                orderItem.setOrderDetailId(orderDetailId);
                orderItem.setBuyingPrice(CartItemObj.getTotalPrice());

//            productId = CartItemObj.getProduct().getId();
                Product product = productService.getProduct(productId);

                orderItem.setProduct(product);
                Discount discount = discountService.getDiscount(discountId);

                orderItem.setDiscount(discount);
                orderItem.setStatus(1);
                orderItem.setCreatedDate(new Date());
                orderItem.setItemCount(CartItemObj.getQuantity());
                //orderItem.setOrderDetail(orderDetail);
                orderItem.setRemarks("New order");
                orderItemList.add(orderItem);
            }
            shoppingService.addOrderItem(orderItemList);
            modelAndView.addObject("orderItemList", orderItemList);
            modelAndView.addObject("userData", userData);
            modelAndView.addObject("orderDetail", orderDetail);
//          modelAndView.addObject("", cartItemList)
//          modelAndView.addObject("cart", cart);        

            modelAndView.setViewName("shopping/receipt");

        }

        return modelAndView;

    }

    //From Narayan
    @RequestMapping(value = {"shopping/search"}, method = RequestMethod.POST)
    public ModelAndView searchProduct(@RequestParam("searchedProductName") String searchedProductName,
            @RequestParam("categoryId") Long categoryId,
            ModelAndView modelAndView) {
        modelAndView.setViewName("shopping/products");
        Product supplier = new Product();
        modelAndView.addObject("supplier", supplier);
        List<Product> allProducts;
        if (categoryId != null) {
            allProducts = productService.getSearchedProductsByCategory(searchedProductName, categoryId);
        } else {
            allProducts = productService.getSearchedProducts(searchedProductName);
        }

        modelAndView.addObject("productList", allProducts);
        modelAndView.addObject("total", allProducts.size());
        modelAndView.addObject("categoryList", categoryService.getAllCategory());
        return modelAndView;
    }

    public boolean doesUserHasAddress(UserData userData) {

        if (userData.getCustomer().getAddress() != null) {
            return true;
        }

        return false;
    }

//    @RequestMapping(value = {"/shopping/receipt"})
//    public ModelAndView shoppingReceipt(ModelAndView modelAndView) {
//        OrderDetail orderDetail = new OrderDetail();
//        modelAndView.addObject("customerReceipt",OrderDetailService.getOrderReceipt());
//        modelAndView.setViewName("shopping/receipt");
//        
//        return modelAndView;
//    }
}
