package com.f1shop.controller;
/*
    F1-SC-C-5: The Discount Controller acts upon the CRUD activities of Discount. It even allows admin to add special offers 
                and sales through it.
*/
import com.f1shop.model.Discount;
import com.f1shop.service.DiscountService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.ModelAndView;


@Controller
public class DiscountController {

    @Autowired
    private DiscountService discountService;

    @RequestMapping(value = {"/admin/alldiscounts", "/admin/discounts"}, method = RequestMethod.GET)
    public ModelAndView allDiscounts(ModelAndView modelAndView) {
        modelAndView.setViewName("discount/list");

        Discount discount = new Discount();
        modelAndView.addObject("discount", discount);
        List<Discount> allDiscounts = discountService.getAllDiscount();
        modelAndView.addObject("discountList", allDiscounts);
        return modelAndView;

    }

    @RequestMapping("/admin/discount/create")
    public String create(Model model) {
        Discount discount = new Discount();
        model.addAttribute(discount);
        return "discount/create";
    }

    @RequestMapping(value = {"admin/discount/create"}, method = RequestMethod.POST)
    public String add(@ModelAttribute("discount") Discount discount) {

        try {
            
            discountService.add(discount);

            return "redirect:/admin/discounts";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/discounts";
        }

    }

    @RequestMapping(value = {"admin/discount/view/{id}"}, method = RequestMethod.GET)
    public String view(@PathVariable("id") final long id, Model model) {
        Discount obj = discountService.getDiscount(id);
        model.addAttribute(obj);
        return "discount/view";
    }

    @RequestMapping(value = {"admin/discount/edit/{id}"}, method = RequestMethod.GET)
    public String edit(@PathVariable("id") final long id, Model model) {
        Discount obj = discountService.getDiscount(id);
        model.addAttribute(obj);
        return "discount/edit";
    }

    @RequestMapping(value = {"admin/discount/edit/{id}"}, method = RequestMethod.POST)
    public String update(@PathVariable("id") final long id, @ModelAttribute("discount") Discount discount) {
        try {
            discount.setId(id);
            discountService.edit(discount);
            return "redirect:/admin/discounts";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/discounts";
        }

    }

    @RequestMapping(value = {"admin/discount/delete/{id}"}, method = RequestMethod.GET)
    public String delete(@PathVariable("id") final long id) {
        Discount obj = discountService.getDiscount(id);
//        obj.setIsActive(false);
        discountService.delete(obj);
//        discountService.edit(obj);
        return "redirect:/admin/discounts";
    }

    @RequestMapping(value = {"/admin/discountSearch"}, method = RequestMethod.POST)
    public ModelAndView search(@RequestParam("searchedDiscountName") String searchedDiscountName, ModelAndView modelAndView) {
        modelAndView.setViewName("discount/list");
        modelAndView.addObject("discountList", discountService.getSearchedDiscounts(searchedDiscountName));
        modelAndView.addObject("filter", searchedDiscountName); 
        return modelAndView;
    }


}
