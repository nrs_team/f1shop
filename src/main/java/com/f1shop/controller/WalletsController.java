package com.f1shop.controller;
/*
    F1-SC-C-11: The Wallet Controller check the waller to the customer. It even acts when customer purchase products from the 
                system.
*/

import com.f1shop.model.Customer;
import com.f1shop.model.UserData;
import com.f1shop.model.Wallet;
import com.f1shop.model.WalletTransaction;
import com.f1shop.service.CustomerService;
import com.f1shop.service.WalletService;
import com.f1shop.service.WalletTransactionService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class WalletsController {

    @Autowired
    private WalletService walletService;

    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private WalletTransactionService walletTransactionService;

    Date date = new Date();

    @RequestMapping(value = {"/admin/allwallets", "/admin/wallets"}, method = RequestMethod.GET)
    public ModelAndView allWallets(ModelAndView modelAndView) {
        modelAndView.setViewName("wallet/list");
        List<Wallet> allcustomerWallet = walletService.getAllWallet();
        modelAndView.addObject("walletList", allcustomerWallet);
        return modelAndView;
    }

    @RequestMapping("/admin/wallet/create")
    public String create(Model model) {

        Wallet wallet = new Wallet();
        List<Customer> customerList = customerService.getAllCustomer();
        model.addAttribute(wallet);
        model.addAttribute(customerList);
        return "wallet/create";
    }

    @RequestMapping(value = {"admin/wallet/create"}, method = RequestMethod.POST)
    public String add(@ModelAttribute("wallet") Wallet wallet) {

        try {
            wallet.setIsActive(true);
            wallet.setCreatedDate(date);
            walletService.add(wallet);

            return "redirect:/admin/wallets";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/wallets";
        }
    }

    @RequestMapping(value = {"admin/wallet/view/{id}"}, method = RequestMethod.GET)
    public String view(@PathVariable("id") final long id, Model model) {
        Wallet obj = walletService.getWallet(id);
        model.addAttribute(obj);
        return "wallet/view";
    }

    @RequestMapping(value = {"admin/wallet/edit/{id}"}, method = RequestMethod.GET)
    public String edit(@PathVariable("id") final long id, Model model) {
        Wallet obj = walletService.getWallet(id);
        model.addAttribute(obj);
        return "wallet/edit";
    }

    @RequestMapping(value = {"admin/wallet/update/{id}"}, method = RequestMethod.POST)
    public String update(@PathVariable("id") final long id, @ModelAttribute("wallet") Wallet wallet) {
        try {
            wallet.setId(id);
            walletService.edit(wallet);
            return "redirect:/admin/wallets";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/admin/wallets";
        }
    }

    @RequestMapping(value = {"admin/wallet/delete/{id}"}, method = RequestMethod.GET)
    public String delete(@PathVariable("id") final long id) {
        Wallet obj = walletService.getWallet(id);
        obj.setIsActive(false);
        walletService.edit(obj);
        return "redirect:/admin/wallets";
    }
//    Code by Rayan
    @RequestMapping(value = {"wallet/topup"}, method = RequestMethod.GET)
    public ModelAndView customertopup(ModelAndView modelAndView) {
       WalletTransaction walletTransaction = new WalletTransaction();
        modelAndView.setViewName("customer/topup");
       modelAndView.addObject(walletTransaction);
        return modelAndView;
    }
}
