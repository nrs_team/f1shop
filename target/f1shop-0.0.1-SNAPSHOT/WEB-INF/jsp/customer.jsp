<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%@ include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>F1 shop</title>
    </head>
    <body>


        <h1>Customers Data</h1>
        <form:form action="customer.do" method="POST" commandName="customer">
            <table>
                <tr>
                    <td>customer ID</td>
                    <td><form:input path="id" /></td>
                </tr>
                <tr>
                    <td>Customer name</td>
                    <td><form:input path="firstName" /></td>
                </tr>
                <tr>
                    <td>Adress</td>
                    <td><form:input path="lastName" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" name="action" value="Add" />
                        <input type="submit" name="action" value="Edit" /> 
                        <input type="submit" name="action" value="Delete" /> 
                        <input type="submit" name="action" value="Search" />
                    </td>
                </tr>
            </table>
        </form:form>
        <br>
        <table border="1">
            <th>ID</th>
            <th>Customer name</th>
            <th>Address</th>
                <c:forEach items="${customerList}" var="customer">
                <tr>
                    <td>${customer.id}</td>
                    <td>${customer.firstName}</td>
                    <td>${customer.lastName}</td>
                </tr>
            </c:forEach>
        </table>


    </body>
</html>