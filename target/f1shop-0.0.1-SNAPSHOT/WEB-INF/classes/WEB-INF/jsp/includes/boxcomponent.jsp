<div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
        
        
        <c:forEach items="${categoryList}" var="category">
            
            <div class="col-lg-4">
                <img class="rounded-circle" src="assests/image/box1.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>${category.name}</h2>
                <p>${category.description}</p>
                <p><a class="btn btn-secondary" href="<spring:url value="/shopping/${category.id}"/>" role="button">View details &raquo;</a></p>
            </div>
        </c:forEach>
        
       
    </div><!-- /.row -->        

</div><!-- /.container -->