



<div class="container">

    <!-- bootstrap-imageupload. -->
    <div class="imageupload panel panel-default">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">Image of Discount</h3>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-default active">File</button>
                <button type="button" class="btn btn-default">URL</button>
            </div>
        </div>
        <div class="file-tab panel-body">
            <div>
                <label class="btn btn-default btn-file">
                    <span>Browse</span>
                    <!-- The file is stored here. -->
                    <c:if test="{$hasImage == 'true'}">
                        
                    </c:if>

                    <input type="file" name="image-file">
                </label>
                <button type="button" class="btn btn-default">Remove</button>
            </div>

        </div>
        <div class="url-tab panel-body">
            <div class="input-group">
                <input type="text" class="form-control hasclear" placeholder="Image URL">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default">Submit</button>
                </div>
            </div>
            <button type="button" class="btn btn-default">Remove</button>
            <!-- The URL is stored here. -->
            <input type="hidden" name="image-url">
        </div>
    </div>

</div>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="<c:url value="/assests/js/bootstrap-imageupload.js" />"></script>

<script>
    var $imageupload = $('.imageupload');
    $imageupload.imageupload();


</script>
